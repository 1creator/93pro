<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/uikit', function () {
    return view('uikit');
});

Route::get('/', function () {
    return view('index');
});

Route::get('/service-group', function () {
    return view('service-group');
});

Route::get('/services', function () {
    return view('services');
});

Route::get('/service-its', function () {
    return view('service-its');
});

Route::get('/service', function () {
    return view('service');
});

Route::get('/international-work', function () {
    return view('international-work');
});

Route::get('/programs', function () {
    return view('programs');
});
Route::get('/program-group', function () {
    return view('program-group');
});

Route::get('/product', function () {
    return view('product');
});

Route::get('/price', function () {
    return view('price');
});

Route::get('/industries', function () {
    return view('industries');
});
Route::get('/industry', function () {
    return view('industry');
});

Route::get('/about', function () {
    return view('about');
});

Route::get('/projects', function () {
    return view('projects');
});

Route::get('/project', function () {
    return view('project');
});

Route::get('/blog', function () {
    return view('articles');
});

Route::get('/article', function () {
    return view('article');
});

Route::get('/offers', function () {
    return view('offers');
});

Route::get('/offer', function () {
    return view('offer');
});

Route::get('/reviews', function () {
    return view('reviews');
});

Route::get('/contacts', function () {
    return view('contacts');
});
