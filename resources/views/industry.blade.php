@extends('layout')
@section('title', "Отрасль")
@section('body')
    <div class="container page-tree">
        <div>
            <a href="#" class="page-tree__item">Главная</a>
            <a href="#" class="page-tree__item">Отраслевые решения</a>
            <a class="page-tree__item">Управления медицинским центром</a>
        </div>
    </div>
    <section class="pt-0 font-weight-light">
        <div class="container">
            <h1 class="pb-20">Управление медицинским центром</h1>
            <div class="mb-80">
                Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы предоставляем
                полный комплекс услуг по обновлению и поддержке программных продуктов на платформе 1С:Предприятие 7.7.
            </div>
            <div class="row">
                <h2 class="mb-3 col-md-8">
                    Увеличивайте прибыль и принимайте
                    управленческие решения быстрее!
                </h2>
            </div>
            <div class="mb-40">
                Поскольку «1С: ERP 2.0» функционирует на новой платформе, возможна удаленная работа с программой (в том
                числе, с мобильных устройств), а также поддержка «облачных» сервисов и технологий. Расширить функционал
                этой системы очень просто, поскольку для этого разработаны многочисленные специализированные решения.
                Для более удобной работы с данной программой предусмотрен особый механизм, позволяющий подключать или
                отключать ее отдельные элементы без изменения общей конфигурации. Таким образом, настройка системы
                отличается гибкостью и удобством.
            </div>
            <div class="row">
                <h2 class="mb-3 col-md-10">
                    Организуйте грамотную работу клиники
                </h2>
            </div>
            <div>
                Поскольку «1С: ERP 2.0» функционирует на новой платформе, возможна удаленная работа с программой (в том
                числе, с мобильных устройств), а также поддержка «облачных» сервисов и технологий. Расширить функционал
                этой системы очень просто, поскольку для этого разработаны многочисленные специализированные решения.
                Для более удобной работы с данной программой предусмотрен особый механизм, позволяющий подключать или
                отключать ее отдельные элементы без изменения общей конфигурации. Таким образом, настройка системы
                отличается гибкостью и удобством.
            </div>
        </div>
    </section>
    <section class="pt-0">
        <div class="container">
            <div class="row no-gutters">
                <div class="col-12 col-md-4">
                    <div class="bg-secondary-faded h-100 p-40 d-flex align-items-center text-center">
                        <div class="h3 px-20">
                            Своевременное
                            обновление
                            конфигураций
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="bg-primary p-40 h-100 d-flex align-items-center text-white text-center">
                        <div class="h3 px-20">
                            Облегчение
                            работы
                            бухгалтерии
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="bg-secondary-faded h-100 p-40 d-flex align-items-center text-center">
                        <div class="h3 px-40">
                            Улучшение
                            качества
                            работы
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="mt-n200"></div>
    @include('sections.you-need-us')
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
    @include('sections.you-can-interested')
    @include('sections.offers-slider')
    @include('sections.you-will-get')
    @include('sections.achievements')
    @include('sections.how-we-works')
    <div class="mt-n200"></div>
    @include('sections.our-projects')
    @include('sections.video-review')
    @include('sections.text-reviews')
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
@endsection