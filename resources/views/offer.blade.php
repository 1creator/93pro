@extends('layout')
@section('title', "Акция")
@section('body')
    <div class="container">
        <div class="page-tree">
            <a href="#" class="page-tree__item">Главная</a>
            <a href="#" class="page-tree__item">Акции</a>
            <a class="page-tree__item">Акция</a>
        </div>
    </div>
    <div class="offer bg-primary text-white mt-n20">
        <div class="container position-relative">
            <div class="row align-items-center">
                <div class="col-12 col-md-8 text-center text-md-left">
                    <div class="offer__content">
                        <h1 class="offer__title">
                            <span class="text-secondary-faded">20% скидка</span><br/>
                            на обслуживание в мае!
                        </h1>
                        <div class="offer__text">
                            Оказывает любые виды услуг по программам.<br/>
                            Также мы предоставляем полный комплекс услуг по обновлению<br/>
                            и поддержке программных продуктов на платформе 1С.
                        </div>
                    </div>
                </div>
                <div class="offer__bg-image mr-n130">
                    <img src="/images/man.png">
                </div>
            </div>
        </div>
    </div>
    <section class="pt-40 pb-0">
        <div class="container">
            <h3 class="mb-3">
                20% скидка
            </h3>
            <div class="text-additional font-weight-light">
                Данная услуга обеспечивает пользователям программ 1С:Предприятие своевременное обновление конфигураций,
                дает право на получение методических рекомендаций и материалов по работе с программой, гарантирует
                полное соответствие учетной системы действующему налоговому законодательству. Данная услуга обеспечивает
                пользователям программ 1С:Предприятие своевременное обновление конфигураци Данная услуга обеспечивает
                пользователям программ 1С:Предприятие своевременное обновление конфигураций, дает право на получение
                методических рекомендаций и материалов по работе с программой, гарантирует полное соответствие учетной
                системы действующему налоговому законодательству. Данная услуга обеспечивает пользователям программ
                1С:Предприятие своевременное обновление конфигураций Данная услуга обеспечивает пользователям программ
                1С:Предприятие своевременное обновление конфигураций, дает право на получение методических рекомендаций
                и материалов по работе с программой, гарантирует полное соответствие учетной системы действующему
                налоговому законодательству. Данная услуга обеспечивает пользователям программ 1С:Предприятие
                своевременное обновление конфигураций
            </div>
        </div>
    </section>
    @include('sections.you-will-get')
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
@endsection