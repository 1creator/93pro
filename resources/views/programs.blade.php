@extends('layout')
@section('title', "Программы")
@section('body')
    <div class="container">
        <div class="page-tree">
            <a href="#" class="page-tree__item">Главная</a>
            <a class="page-tree__item">Программы</a>
        </div>
    </div>
    @include('sections.programs')
    <div class="mt-n200"></div>
    @include('sections.we-will-call-93')
    <div class="mt-n200"></div>
    @include('sections.industry-cases')
    @include('sections.you-can-interested')
    @include('sections.offers-slider')
    @include('sections.advantages')
    @include('sections.achievements')
    <div class="mb-200"></div>
    @include('sections.video-reviews')
    @include('sections.text-reviews')
    <div class="mb-n200"></div>
    @include('sections.we-will-call')
@endsection