@extends('layout')
@section('title', "Контакты")
@section('body')
    <div class="container">
        <div class="page-tree">
            <a href="#" class="page-tree__item">Главная</a>
            <a class="page-tree__item">Контакты</a>
        </div>
    </div>
    <section class="pt-0">
        <div class="container contacts">
            <h1 class="mb-80">Контакты</h1>
            <div class="mb-130" id="map" style="width: 100%;height: 500px"></div>
            <div class="row mb-40">
                <div class="col-12 col-md-4 mb-40 mb-md-0">
                    <div class="d-flex">
                        <i class="icon-location text-secondary-faded"></i>
                        <div>
                            <div class="h3 contacts__location">
                                115280, г. Москва,
                            </div>
                            <div class="mb-20">
                                115280, г. Москва,
                                м. Электрозаводская,
                                площадь Журавлева 2,
                                строение 2, офис 329
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-40 mb-md-0">
                    <div class="d-flex">
                        <i class="icon-phone text-secondary-faded"></i>
                        <div>
                            <div class="h3 mb-20 contacts__phones">
                                <div class="mb-2">8 999 807 30 16</div>
                                <div>8 999 840 20 30</div>
                            </div>
                            <div class="d-flex justify-content-between">
                                <a class="pr-2 text-secondary" href="/#"><i class="icon-call"></i></a>
                                <a class="px-2 text-secondary" href="/#"><i class="icon-telegram"></i></a>
                                <a class="px-2 text-secondary" href="/#"><i class="icon-whatsapp"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-4 mb-40 mb-md-0">
                    <div class="d-flex">
                        <i class="icon-mail text-secondary-faded"></i>
                        <div>
                            <div class="h3 mb-20 contacts__phones">
                                info@drgrp.ru
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pl-40">
                <h3>Реквизиты</h3>
                <div>
                    ООО «ДИАР Групп»<br/>
                    ИНН 7723871041 КПП 772301001<br/>
                    ОГРН 1137746381882<br/>
                    р/с 40702810538250017147<br/>
                    в ОАО «Сбербанк России» г. Москва
                </div>
            </div>
        </div>
    </section>
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
    <script>
        function initMap() {
            var point = {lat: 55.784133, lng: 37.701634};
            var map = new google.maps.Map(
                document.getElementById('map'), {zoom: 17, center: point});
            var marker = new google.maps.Marker({position: point, map: map});
        }
    </script>
    <script async defer
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD5ZdnnqSGfLxCSm55llDgF8liCxoZGOhk&callback=initMap">
    </script>
@endsection