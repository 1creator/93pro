@extends('layout')
@section('title', "Статьи")
@section('body')
    <div class="container">
        <div class="page-tree">
            <a href="#" class="page-tree__item">Главная</a>
            <a class="page-tree__item">Блог</a>
        </div>
    </div>
    <section class="py-0">
        <div class="container mb-md-n30">
            <h1 class="mb-70">Полезно знать</h1>
            <div class="row">
                <div class="col-12 col-md-7 mb-30">
                    <div class="card-article-preview bg-dark">
                        <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                        <div class="card-article-preview__caption">
                            Нужны ли услуги программиста 1С? Разумеется, нет! Кому нужны
                            отлаженные бизнес-процессы
                        </div>
                        <div class="card-article-preview__text">
                            Уверенность в стабильной работе программы и индивидуальные
                            решения для бизнеса, которые сэкономят деньги и рабочее время
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="/article" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                            <div><i class="icon-eye"></i>
                                <small>2144</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 mb-30">
                    <div class="card-article-preview bg-secondary-faded">
                        <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                        <div class="card-article-preview__caption">
                            Нужны ли услуги1С?
                            Разумеется, нет!
                        </div>
                        <div class="card-article-preview__text">
                            Уверенность в стабильной работе программы и индивидуальные
                            решения для бизнеса, которые сэкономят деньги и рабочее время
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="/article" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                            <div><i class="icon-eye"></i>
                                <small>2144</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-5 mb-30">
                    <div class="card-article-preview bg-dark">
                        <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                        <div class="card-article-preview__caption">
                            Нужны ли услуги1С?
                            Разумеется, нет!
                        </div>
                        <div class="card-article-preview__text">
                            Уверенность в стабильной работе программы и индивидуальные
                            решения для бизнеса, которые сэкономят деньги и рабочее время
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="/article" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                            <div><i class="icon-eye"></i>
                                <small>2144</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-7 mb-30">
                    <div class="card-article-preview bg-dark card-article-preview--bg-img"
                         style="background-image: url('./images/bg-office-work.jpg')">
                        <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                        <div class="card-article-preview__caption">
                            Нужны ли услуги программиста 1С? Разумеется, нет! Кому нужны
                            отлаженные бизнес-процессы
                        </div>
                        <div class="card-article-preview__text">
                            Уверенность в стабильной работе программы и индивидуальные
                            решения для бизнеса, которые сэкономят деньги и рабочее время
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="/article" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                            <div><i class="icon-eye"></i>
                                <small>2144</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-7 mb-30">
                    <div class="card-article-preview bg-dark">
                        <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                        <div class="card-article-preview__caption">
                            Нужны ли услуги программиста 1С? Разумеется, нет! Кому нужны
                            отлаженные бизнес-процессы
                        </div>
                        <div class="card-article-preview__text">
                            Уверенность в стабильной работе программы и индивидуальные
                            решения для бизнеса, которые сэкономят деньги и рабочее время
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="/article" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                            <div><i class="icon-eye"></i>
                                <small>2144</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 mb-30">
                    <div class="card-article-preview bg-secondary-faded">
                        <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                        <div class="card-article-preview__caption">
                            Нужны ли услуги1С?
                            Разумеется, нет!
                        </div>
                        <div class="card-article-preview__text">
                            Уверенность в стабильной работе программы и индивидуальные
                            решения для бизнеса, которые сэкономят деньги и рабочее время
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="/article" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                            <div><i class="icon-eye"></i>
                                <small>2144</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-5 mb-30">
                    <div class="card-article-preview bg-dark">
                        <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                        <div class="card-article-preview__caption">
                            Нужны ли услуги1С?
                            Разумеется, нет!
                        </div>
                        <div class="card-article-preview__text">
                            Уверенность в стабильной работе программы и индивидуальные
                            решения для бизнеса, которые сэкономят деньги и рабочее время
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="/article" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                            <div><i class="icon-eye"></i>
                                <small>2144</small>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-7 mb-30">
                    <div class="card-article-preview bg-dark card-article-preview--bg-img"
                         style="background-image: url('./images/bg-office-work.jpg')">
                        <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                        <div class="card-article-preview__caption">
                            Нужны ли услуги программиста 1С? Разумеется, нет! Кому нужны
                            отлаженные бизнес-процессы
                        </div>
                        <div class="card-article-preview__text">
                            Уверенность в стабильной работе программы и индивидуальные
                            решения для бизнеса, которые сэкономят деньги и рабочее время
                        </div>
                        <div class="d-flex justify-content-between align-items-center">
                            <a href="/article" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                            <div><i class="icon-eye"></i>
                                <small>2144</small>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <nav>
                <ul class="pagination pagination-sm justify-content-end">
                    <li class="page-item disabled">
                        <span class="page-link">Предыдущая</span>
                    </li>
                    <li class="page-item active"><span class="page-link">1</span></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#">Следующая</a>
                    </li>
                </ul>
            </nav>
        </div>
    </section>
    @include('sections.you-will-get')
    <div class="mb-n200"></div>
    @include('sections.we-will-call')
@endsection