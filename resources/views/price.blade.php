@extends('layout')
@section('title', "Прайс")
@section('body')
    <div class="container page-tree">
        <div>
            <a href="#" class="page-tree__item">Главная</a>
            <a href="#" class="page-tree__item">Программы</a>
            <a class="page-tree__item">Прайс</a>
        </div>
    </div>
    <div class="mt-n200"></div>
    @include('sections.price')
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
    <div class="mt-n200"></div>
    @include('sections.you-will-get')
    @include('sections.achievements')
    @include('sections.we-will-call')
@endsection