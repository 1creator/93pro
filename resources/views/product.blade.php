@extends('layout')
@section('title', "Продукт")
@section('body')
    <div class="container page-tree page-tree--absolute">
        <div>
            <a href="#" class="page-tree__item">Главная</a>
            <a href="#" class="page-tree__item">Услуги</a>
            <a href="#" class="page-tree__item">Сопровождение</a>
            <a class="page-tree__item">ИТС</a>
        </div>
    </div>
    <section class="section-header" style="background-image: url('./images/bg-conversation.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-7 text-white header-slider">
                    <h1 class="mb-20">1C: Предприятие 8.ERP</h1>
                    <div>
                        Оказывает любые виды услуг по программам.<br class="d-none d-lg-block"/>
                        Также мы предоставляем полный комплекс услуг<br class="d-none d-lg-block"/>
                        по обновлению и поддержке программных продуктов на платформе 1С.
                    </div>
                </div>
                <div class="col-12 col-md-5">
                    <form class="form-validation" action="/test" method="post" novalidate>
                        <div class="form__content">
                            <div class="input-validation w-100 mb-3">
                                <input class="form-control mb-4 text-white" placeholder="Имя" required>
                                <i class="input-validation__icon"></i>
                            </div>
                            <div class="input-validation w-100 mb-3">
                                <input class="form-control mb-4 text-white" placeholder="Номер телефона" type="tel"
                                       required>
                                <i class="input-validation__icon"></i>
                            </div>
                            <div class="custom-control custom-checkbox checkbox-terms mb-4">
                                <input type="checkbox" checked class="custom-control-input" id="header-cb-terms-1"
                                       required>
                                <label class="custom-control-label " for="header-cb-terms-1">
                                    Я соглашаюсь с
                                    <a href="/terms" class="text-secondary-faded" target="_blank">политикой
                                        конфиденциальности</a>
                                </label>
                            </div>
                            <button class="btn btn-sm btn-secondary-faded" type="submit">Заказать звонок</button>
                        </div>
                        <div class="form__success-message text-white">
                            <div class="h2 text-secondary-faded">Спасибо за обращение!</div>
                            <div>Ваша заявка принята и скоро мы с вами свяжемся!</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @include('sections.how-it-works')
    <div class="mt-n200"></div>
    @include('sections.you-need-us')
    <section class="pt-0 pb-200">
        <div class="container">
            <h2 class="h1">Стоимость договора ИТС</h2>
            <table class="table table-borderless mb-5">
                <tbody>
                <tr>
                    <td>
                        <div class="h3 text-primary text-left">ИТС ПРОФ Комфортный</div>
                    </td>
                    <td>
                        <div class="text-nowrap text-left">1 месяц</div>
                    </td>
                    <td>
                        <div class="font-weight-bold">от 1 000р.</div>
                    </td>
                    <td class="text-right">
                        <button class="btn btn-secondary-faded px-50"
                                data-toggle="modal" data-target="#product-modal-1">Выбрать
                        </button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="h3 text-primary text-left">ИТС ПРОФ Комфортный</div>
                    </td>
                    <td>
                        <div class="text-nowrap text-left">3 месяца</div>
                    </td>
                    <td>
                        <div class="font-weight-bold">от 1 000р.</div>
                    </td>
                    <td class="text-right">
                        <button class="btn btn-secondary-faded px-50"
                                data-toggle="modal" data-target="#product-modal-1">Выбрать
                        </button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="h3 text-primary text-left">ИТС ПРОФ Комфортный</div>
                    </td>
                    <td>
                        <div class="text-nowrap text-left">5 месяцев</div>
                    </td>
                    <td>
                        <div class="font-weight-bold">от 1 000р.</div>
                    </td>
                    <td class="text-right">
                        <button class="btn btn-secondary-faded px-50"
                                data-toggle="modal" data-target="#product-modal-1">Выбрать
                        </button>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="h3 text-primary text-left">ИТС ПРОФ Комфортный</div>
                    </td>
                    <td>
                        <div class="text-nowrap text-left">24 месяца</div>
                    </td>
                    <td>
                        <div class="font-weight-bold">от 1 000р.</div>
                    </td>
                    <td class="text-right">
                        <button class="btn btn-secondary-faded px-50"
                                data-toggle="modal" data-target="#product-modal-1">Выбрать
                        </button>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="modal fade" id="product-modal-1" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div class="modal-content">
                    <form action="/test" method="post" class="form-validation" novalidate>
                        <div class="form__content">
                            <div class="modal-header">
                                <div class="h2 modal-title text-primary mb-3">1C: Предприятие 8.ERP</div>
                                <div class="font-weight-bold" style="font-size: 24px">115 000 р.</div>
                            </div>
                            <div class="modal-body">
                                <div style="font-size: 16px" class="d-flex">
                                    <div class="mx-4">
                                        <div class="custom-control custom-radio mb-4">
                                            <input type="radio" id="key-hasp" name="key-type" required
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="key-hasp">HASP</label>
                                        </div>
                                        <div class="custom-control custom-radio mb-40">
                                            <input type="radio" id="key-digital" name="key-type" required
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="key-digital">цифровой</label>
                                        </div>
                                    </div>
                                    <div class="mx-4">
                                        <div class="custom-control custom-radio mb-4">
                                            <input type="radio" id="key-physical" name="key-type" required
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="key-physical">физический</label>
                                        </div>
                                        <div class="custom-control custom-radio mb-40">
                                            <input type="radio" id="key-license" name="key-type" required
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="key-license">лицензионный
                                                ключ</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="input-validation w-100 mb-3">
                                    <input class="form-control" type="text" placeholder="Имя" required>
                                    <i class="input-validation__icon"></i>
                                </div>
                                <div class="input-validation w-100 mb-3">
                                    <input class="form-control" type="email" placeholder="Email" required>
                                    <i class="input-validation__icon"></i>
                                </div>
                                <div class="input-validation w-100 mb-40">
                                    <input class="form-control" type="tel" placeholder="Номер телефона" required>
                                    <i class="input-validation__icon"></i>
                                </div>
                                <div class="custom-control custom-checkbox checkbox-terms mb-40">
                                    <input type="checkbox" class="custom-control-input" id="cb-terms-2" required>
                                    <label class="custom-control-label" for="cb-terms-2">
                                        Я соглашаюсь с
                                        <a href="/terms" class="text-secondary-faded" target="_blank">политикой
                                            конфиденциальности</a>
                                    </label>
                                </div>
                                <button class="btn btn-secondary-faded">Заказать звонок</button>
                            </div>
                        </div>
                        <div class="form__success-message text-center">
                            <div class="h2 modal-title text-primary">Спасибо за обращение!</div>
                            <div>
                                Ваша заявка принята и скоро мы с вами свяжемся!
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
    @include('sections.you-can-interested')
    @include('sections.offers-slider')
    @include('sections.you-will-get')
    @include('sections.achievements')
    @include('sections.how-we-works')
    <div class="mt-n200"></div>
    @include('sections.our-projects')
    @include('sections.video-review')
    @include('sections.text-reviews')
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
@endsection