@extends('layout')
@section('title', "О компании")
@section('body')
    <div class="container page-tree page-tree--absolute">
        <div>
            <a href="#" class="page-tree__item">Главная</a>
            <a class="page-tree__item">О компании</a>
        </div>
    </div>
    <section class="section-header py-200" style="background-image: url('./images/bg-about.jpg')">
        <div class="container position-relative text-white">
            <h1 class="mb-20">Команда Programs93</h1>
            <div class="row">
                <div class="col-12 col-md-6">
                    <div class="font-weight-light">
                        Оказывает любые виды услуг по программам.<br class="d-none d-lg-block"/>
                        Также мы предоставляем полный комплекс услуг<br class="d-none d-lg-block"/>
                        по обновлению и поддержке программных продуктов на платформе 1С.
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="mt-n130"></div>
    @include('sections.who-are-we')
    @include('sections.achievements')
    <section class="pt-200 pb-0">
        <div class="py-40">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-12 col-md-7">
                        <div class="h1 mb-3">Наша команда</div>
                        <div class="font-weight-light" style="letter-spacing: 1px;">
                            Оказывает любые виды услуг по программам.<br>
                            Также мы предоставляем полный комплекс услуг по обновлению<br>
                            и поддержке программных продуктов на платформе 1С.
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="mb-130">
                <div class="h2 mb-50">Отдел 1</div>
                <div class="d-flex flex-wrap mb-80"
                     onclick="goToSpecialistCard('specialist-card-1');">
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-1" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-1" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-1" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-1" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-1" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-1" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-1" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-1" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-1" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                </div>
                <a name="specialist-card-1"></a>
                <div class="tab-content">
                    <div class="tab-pane fade" id="specialist-1" role="tabpanel">
                        <div class="card shadow-lg specialist-card mb-40">
                            <div class="card-body">
                                <div class="row no-gutters">
                                    <div class="col-12 col-lg-3 d-flex align-items-end mb-md-n20">
                                        <img src="./images/team-participant.png">
                                    </div>
                                    <div class="col-12 col-lg-5 d-flex flex-column justify-content-center">
                                        <div class="h3 mb-30">Руководитель отдела</div>
                                        <div class="line-75 mb-30"></div>
                                        <div class="h2 mb-30">Петров Александр</div>
                                        <div class="mb-20">Обеспечение порядка</div>
                                        <div class="small text-gray font-weight-light">
                                            <div class="mb-20">
                                                <span class="font-weight-bold">Опыт работы:</span> 10 лет
                                            </div>
                                            <div class="mb-20">
                                                <span class="font-weight-bold">Навыки:</span>
                                                Проходят серьезное обучение<br/>
                                                Высококвалифицированны<br/>
                                                Особый подход
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 d-flex align-items-end">
                                        <div class="glide" data-glide-perview="2">
                                            <div class="glide__track" data-glide-el="track">
                                                <ul class="glide__slides">
                                                    <li class="glide__slide">
                                                        <a data-fancybox href="./images/certificate.png">
                                                            <img src="./images/certificate.png"></a>
                                                    </li>
                                                    <li class="glide__slide">
                                                        <a data-fancybox href="./images/certificate.png">
                                                            <img src="./images/certificate.png"></a>
                                                    </li>
                                                </ul>
                                                <div data-glide-el="controls" class="text-center">
                                                    <button data-glide-dir="<"
                                                            class="btn btn-sm btn-secondary-faded btn-circle mr-2">
                                                        <i class="icon-arrow-left"></i>
                                                    </button>
                                                    <button data-glide-dir=">"
                                                            class="btn btn-sm btn-secondary-faded btn-circle ">
                                                        <i class="icon-arrow-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-secondary-faded">Получить консультацию</button>
                    </div>
                </div>
            </div>
            <div class="mb-130">
                <div class="h2 mb-50">Отдел 2</div>
                <div class="d-flex flex-wrap mb-40"
                     onclick="goToSpecialistCard('specialist-card-2');">
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-2" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                </div>
                <div class="text-center mb-80">
                    <a href="#" class="btn btn-link">СМОТРЕТЬ ВСЕХ</a>
                </div>
                <a name="specialist-card-2"></a>
                <div class="tab-content">
                    <div class="tab-pane fade" id="specialist-2" role="tabpanel">
                        <div class="card shadow-lg specialist-card mb-40">
                            <div class="card-body">
                                <div class="row no-gutters">
                                    <div class="col-12 col-lg-3 d-flex align-items-end mb-md-n20">
                                        <img src="./images/team-participant.png">
                                    </div>
                                    <div class="col-12 col-lg-5 d-flex flex-column justify-content-center">
                                        <div class="h3 mb-30">Руководитель отдела</div>
                                        <div class="line-75 mb-30"></div>
                                        <div class="h2 mb-30">Петров Александр</div>
                                        <div class="mb-20">Обеспечение порядка</div>
                                        <div class="small text-gray font-weight-light">
                                            <div class="mb-20">
                                                <span class="font-weight-bold">Опыт работы:</span> 10 лет
                                            </div>
                                            <div class="mb-20">
                                                <span class="font-weight-bold">Навыки:</span>
                                                Проходят серьезное обучение<br/>
                                                Высококвалифицированны<br/>
                                                Особый подход
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 d-flex align-items-end">
                                        <div class="glide" data-glide-perview="2">
                                            <div class="glide__track" data-glide-el="track">
                                                <ul class="glide__slides">
                                                    <li class="glide__slide">
                                                        <a data-fancybox href="./images/certificate.png">
                                                            <img src="./images/certificate.png"></a>
                                                    </li>
                                                    <li class="glide__slide">
                                                        <a data-fancybox href="./images/certificate.png">
                                                            <img src="./images/certificate.png"></a>
                                                    </li>
                                                </ul>
                                                <div data-glide-el="controls" class="text-center">
                                                    <button data-glide-dir="<"
                                                            class="btn btn-sm btn-secondary-faded btn-circle mr-2">
                                                        <i class="icon-arrow-left"></i>
                                                    </button>
                                                    <button data-glide-dir=">"
                                                            class="btn btn-sm btn-secondary-faded btn-circle ">
                                                        <i class="icon-arrow-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-secondary-faded">Получить консультацию</button>
                    </div>
                </div>
            </div>
            <div>
                <div class="h2 mb-50">Отдел 3</div>
                <div class="d-flex flex-wrap mb-80"
                     onclick="goToSpecialistCard('specialist-card-3');">
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                    <div class="team-participant cursor-pointer"
                         style="background-image: url('./images/team-man.png')"
                         data-toggle="tab" data-target="#specialist-3" data-role="tab">
                        <div class="team-participant__about">
                            <div class="h4">Петров Александр</div>
                            <div>Руководитель отдела</div>
                        </div>
                    </div>
                </div>
                <a name="specialist-card-3"></a>
                <div class="tab-content">
                    <div class="tab-pane fade" id="specialist-3" role="tabpanel">
                        <div class="card shadow-lg specialist-card mb-40">
                            <div class="card-body">
                                <div class="row no-gutters">
                                    <div class="col-12 col-lg-3 d-flex align-items-end mb-md-n20">
                                        <img src="./images/team-participant.png">
                                    </div>
                                    <div class="col-12 col-lg-5 d-flex flex-column justify-content-center">
                                        <div class="h3 mb-30">Руководитель отдела</div>
                                        <div class="line-75 mb-30"></div>
                                        <div class="h2 mb-30">Петров Александр</div>
                                        <div class="mb-20">Обеспечение порядка</div>
                                        <div class="small text-gray font-weight-light">
                                            <div class="mb-20">
                                                <span class="font-weight-bold">Опыт работы:</span> 10 лет
                                            </div>
                                            <div class="mb-20">
                                                <span class="font-weight-bold">Навыки:</span>
                                                Проходят серьезное обучение<br/>
                                                Высококвалифицированны<br/>
                                                Особый подход
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-lg-4 d-flex align-items-end">
                                        <div class="glide" data-glide-perview="2">
                                            <div class="glide__track" data-glide-el="track">
                                                <ul class="glide__slides">
                                                    <li class="glide__slide">
                                                        <a data-fancybox href="./images/certificate.png">
                                                            <img src="./images/certificate.png"></a>
                                                    </li>
                                                    <li class="glide__slide">
                                                        <a data-fancybox href="./images/certificate.png">
                                                            <img src="./images/certificate.png"></a>
                                                    </li>
                                                </ul>
                                                <div data-glide-el="controls" class="text-center">
                                                    <button data-glide-dir="<"
                                                            class="btn btn-sm btn-secondary-faded btn-circle mr-2">
                                                        <i class="icon-arrow-left"></i>
                                                    </button>
                                                    <button data-glide-dir=">"
                                                            class="btn btn-sm btn-secondary-faded btn-circle ">
                                                        <i class="icon-arrow-right"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-secondary-faded">Получить консультацию</button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="py-200">
        <div class="container">
            <h3 class="h1 mb-20">Наши достижения</h3>
            <div class="row">
                <div class="col-12 col-md-10">
                    <div class="mb-80 font-weight-light">
                        Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы
                        предоставляем
                        полный комплекс услуг по обновлению и поддержке программных продуктов на платформе
                        1С:Предприятие 7.7.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-5 offset-md-1 text-center text-white pr-md-30">
                    <h3 class="h2 text-additional mb-20 font-weight-black">82 награды</h3>
                    <div class="text-center mb-40">
                        <a href="#">Смотреть отзывы</a>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="bg-secondary-faded py-60">
                                <div class="h1 text-dark mb-20">250</div>
                                <div>Сертификатов,
                                    подтверждаюих
                                    компетенции
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="bg-dark py-60">
                                <div class="h1 text-secondary-faded mb-20">250</div>
                                <div>Сертификатов,
                                    подтверждаюих
                                    компетенции
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 text-center text-white pl-md-30">
                    <div class="row mb-40">
                        <div class="col">
                            <div class="bg-dark py-60">
                                <div class="h1 text-secondary-faded mb-20">250</div>
                                <div>Сертификатов,
                                    подтверждающих
                                    компетенции
                                </div>
                            </div>
                        </div>
                        <div class="col">
                            <div class="bg-secondary-faded py-60">
                                <div class="h1 text-dark mb-20">250</div>
                                <div>Сертификатов,
                                    подтверждаюpих
                                    компетенции
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="h2 text-additional mb-20 font-weight-black">42 сертификата</div>
                    <div class="text-center">
                        <a href="#">Смотреть отзывы</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('sections.93-divider')
    @include('sections.you-will-get')
    <div class="mt-n200"></div>
    @include('sections.our-projects')
    @include('sections.video-review')
    @include('sections.text-reviews')
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
@endsection