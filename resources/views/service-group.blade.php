@extends('layout')
@section('title', "Группа услуг")
@section('body')
    <div class="container">
        <div class="page-tree">
            <a href="#" class="page-tree__item">Главная</a>
            <a href="#" class="page-tree__item">Услуги</a>
            <a class="page-tree__item">Внедрение</a>
        </div>
    </div>
    <div class="container">
        <h1 class="mb-20">Внедрение 1С</h1>
        <div class="row mb-80">
            <div class="col-12 col-lg-10">
                Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы
                предоставляем полный
                комплекс услуг по обновлению и поддержке программных продуктов на платформе 1С:Предприятие 7.7.
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-secondary-faded">
                    <div class="card-body">
                        <div class="h2 card-title">Внедрение 1С</div>
                        <button class="btn btn-dark btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-primary">
                    <div class="card-body">
                        <div class="h2 card-title">Поддержка по SLA</div>
                            <button class="btn btn-light btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sections.we-will-call-93')
    <div class="mt-n200"></div>
    @include('sections.industry-cases')
    @include('sections.you-can-interested')
    @include('sections.offers-slider')
    @include('sections.advantages')
    @include('sections.achievements')
    <div class="mb-200"></div>
    @include('sections.video-reviews')
    @include('sections.text-reviews')
    <div class="mb-n200"></div>
    @include('sections.we-will-call')
@endsection