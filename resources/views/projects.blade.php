@extends('layout')
@section('title', "Проекты")
@section('body')
    <div class="container page-tree">
        <div>
            <a href="#" class="page-tree__item">Главная</a>
            <a class="page-tree__item">Проекты</a>
        </div>
    </div>
    <section class="py-0">
        <div class="container">
            <div class="row text-primary mb-70">
                <div class="col-12 col-md-7">
                    <h1>Наши проекты</h1>
                </div>
                <div class="col-12 col-md-5 nav nav-tabs border-0 justify-content-end pr-1" role="tablist"
                     style="margin-top: 11px">
                    <a href="#integration" class="nav-item btn btn-link py-0 px-2 active"
                       data-toggle="tab"
                       role="tab">ВНЕДРЕНИЕ</a>
                    <a href="#creating" class="nav-item btn btn-link py-0 px-2"
                       data-toggle="tab"
                       role="tab">СОЗДАНИЕ</a>
                    <a href="#support" class="nav-item btn btn-link py-0 px-2"
                       data-toggle="tab"
                       role="tab">СОПРОВОЖДЕНИЕ</a>
                    <a href="#integration" class="nav-item btn btn-link py-0 px-2"
                       data-toggle="tab"
                       role="tab">ВНЕДРЕНИЕ</a>
                    <a href="#creating" class="nav-item btn btn-link py-0 px-2"
                       data-toggle="tab"
                       role="tab">СОЗДАНИЕ</a>
                    <a href="#support" class="nav-item btn btn-link py-0 px-2"
                       data-toggle="tab"
                       role="tab">СОПРОВОЖДЕНИЕ</a>
                </div>
            </div>
        </div>
        <div class="container tab-content">
            <div class="tab-pane fade show active" id="integration" role="tabpanel">
                <div class="project-preview" style="background-image: url('./images/bg-office-work.jpg')">
                    <div class="project-preview__header">
                        <div class="project-preview-header__date">
                            <div class="h1">21</div>
                            <div>Января 2019</div>
                        </div>
                        <div class="project-preview-header__title">
                            <div class="h2">Внедрение 1С</div>
                            <div>ОАО "Воентелеком"</div>
                            <a href="/project" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                        </div>
                        <div class="project-preview-header__stars ml-auto">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                        </div>
                    </div>
                    <div class="project-preview-body project-preview-body--no-line">
                        <div class="h3 project-preview-body__header">
                            Нужны ли услуги программиста 1С? Разумеется, нет! Кому
                            нужны отлаженные бизнес-процессы</div>
                        <div class="project-preview-body__text">
                            Уверенность в стабильной работе программы и
                            индивидуальные&nbsp;решения для бизнеса, которые сэкономят&nbsp;деньги
                            и рабочее время
                        </div>
                    </div>
                </div>
                <nav class="mt-30">
                    <ul class="pagination pagination-sm justify-content-end">
                        <li class="page-item disabled">
                            <span class="page-link">Предыдущая</span>
                        </li>
                        <li class="page-item active"><span class="page-link">1</span></li>
                        <li class="page-item"><a class="page-link" href="#">2</a></li>
                        <li class="page-item"><a class="page-link" href="#">3</a></li>
                        <li class="page-item">
                            <a class="page-link" href="#">Следующая</a>
                        </li>
                    </ul>
                </nav>
            </div>
            <div class="tab-pane fade" id="creating" role="tabpanel">
                <div class="project-preview" style="background-image: url('./images/bg-office-work.jpg')">
                    <div class="project-preview__header">
                        <div class="project-preview-header__date">
                            <div class="h1">21</div>
                            <div>Января 2019</div>
                        </div>
                        <div class="project-preview-header__title">
                            <div class="h2">Внедрение 1С</div>
                            <div>ОАО "Воентелеком"</div>
                            <a href="/project" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                        </div>
                        <div class="project-preview-header__stars ml-auto">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                        </div>
                    </div>
                    <div class="project-preview-body project-preview-body--no-line">
                        <div class="h3 project-preview-body__header">
                            Нужны ли услуги программиста 1С? Разумеется, нет! Кому
                            нужны отлаженные бизнес-процессы</div>
                        <div class="project-preview-body__text">
                            Уверенность в стабильной работе программы и
                            индивидуальные&nbsp;решения для бизнеса, которые сэкономят&nbsp;деньги
                            и рабочее время
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="support" role="tabpanel">
                <div class="project-preview" style="background-image: url('./images/bg-office-work.jpg')">
                    <div class="project-preview__header">
                        <div class="project-preview-header__date">
                            <div class="h1">21</div>
                            <div>Января 2019</div>
                        </div>
                        <div class="project-preview-header__title">
                            <div class="h2">Внедрение 1С</div>
                            <div>ОАО "Воентелеком"</div>
                            <a href="/project" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                        </div>
                        <div class="project-preview-header__stars ml-auto">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                            <img src="./images/star.svg">
                        </div>
                    </div>
                    <div class="project-preview-body project-preview-body--no-line">
                        <div class="h3 project-preview-body__header">
                            Нужны ли услуги программиста 1С? Разумеется, нет! Кому
                            нужны отлаженные бизнес-процессы</div>
                        <div class="project-preview-body__text">
                            Уверенность в стабильной работе программы и
                            индивидуальные&nbsp;решения для бизнеса, которые сэкономят&nbsp;деньги
                            и рабочее время
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('sections.you-will-get')
    <div class="mt-n200"></div>
    @include('sections.text-reviews')
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
@endsection