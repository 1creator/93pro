@extends('layout')
@section('title', "Услуга ИТС")
@section('body')
    <div class="container page-tree page-tree--absolute">
        <div>
            <a href="#" class="page-tree__item">Главная</a>
            <a href="#" class="page-tree__item">Услуги</a>
            <a href="#" class="page-tree__item">Сопровождение</a>
            <a class="page-tree__item">ИТС</a>
        </div>
    </div>
    <section class="section-header" style="background-image: url('./images/bg-conversation.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 text-white header-slider mb-4 mb-md-0">
                    <h1 class="mb-20">1С: ИТС </h1>
                    <div style="font-size: 18px; letter-spacing: 0.6px">
                        Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы предоставляем полный комплекс услуг по обновлению и поддержке программных продуктов на платформе 1С:Предприятие 7.7.
                    </div>
                </div>
                <div class="col-12 col-md-5 offset-md-1">
                    <form class="form-validation" action="/test" method="post" novalidate>
                        <div class="form__content">
                            <div class="input-validation w-100 mb-3">
                                <input class="form-control mb-4 text-white" placeholder="Имя" required>
                                <i class="input-validation__icon"></i>
                            </div>
                            <div class="input-validation w-100 mb-3">
                                <input class="form-control mb-4 text-white" placeholder="Номер телефона" type="tel"
                                       required>
                                <i class="input-validation__icon"></i>
                            </div>
                            <div class="custom-control custom-checkbox checkbox-terms mb-4">
                                <input type="checkbox" checked class="custom-control-input" id="header-cb-terms-1"
                                       required>
                                <label class="custom-control-label " for="header-cb-terms-1">
                                    Я соглашаюсь с
                                    <a href="/terms" class="text-secondary-faded" target="_blank">политикой
                                        конфиденциальности</a>
                                </label>
                            </div>
                            <button class="btn btn-sm btn-secondary-faded" type="submit">Заказать звонок</button>
                        </div>
                        <div class="form__success-message text-white">
                            <div class="h2 text-secondary-faded">Спасибо за обращение!</div>
                            <div>Ваша заявка принята и скоро мы с вами свяжемся!</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    @include('sections.how-it-works')
    <div class="mb-n200"></div>
    @include('sections.price')
    <div class="mb-n200"></div>
    @include('sections.we-will-call')
    <div class="mb-n200"></div>
    @include('sections.fare-details')
    @include('sections.white-blocks')
    @include('sections.we-will-call')
    @include('sections.you-can-interested')
    @include('sections.offers-slider')
    @include('sections.you-will-get')
    @include('sections.achievements')
    @include('sections.how-we-works')
    @include('sections.our-projects')
    @include('sections.video-review')
    @include('sections.text-reviews')
    <div class="mb-n200"></div>
    @include('sections.we-will-call')
@endsection