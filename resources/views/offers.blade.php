@extends('layout')
@section('title', "Акции")
@section('body')
    <div class="container">
        <div class="page-tree">
            <a href="#" class="page-tree__item">Главная</a>
            <a class="page-tree__item">Акции</a>
        </div>
    </div>
    <div class="offer bg-primary text-white mt-n20">
        <div class="container position-relative">
            <div class="row align-items-center">
                <div class="col-12 col-md-8 text-center text-md-left">
                    <div class="offer__content">
                        <div class="h1 offer__title">
                            <span class="text-secondary-faded">20% скидка</span><br/>
                            на обслуживание в мае!
                        </div>
                        <div class="offer__text">
                            Оказывает любые виды услуг по программам.<br/>
                            Также мы предоставляем полный комплекс услуг по обновлению<br/>
                            и поддержке программных продуктов на платформе 1С.
                        </div>
                        <a href="/offers" class="btn btn-secondary-faded offer__btn">Акция</a>
                    </div>
                </div>
                <div class="offer__bg-image mr-n130">
                    <img src="/images/man.png">
                </div>
            </div>
        </div>
    </div>
    <div class="mb-30"></div>
    <div class="offer bg-primary text-white">
        <div class="container position-relative">
            <div class="row align-items-center">
                <div class="col-12 col-md-8 text-center text-md-left">
                    <div class="offer__content">
                        <div class="h1 offer__title">
                            <span class="text-secondary-faded">20% скидка</span><br/>
                            на обслуживание в мае!
                        </div>
                        <div class="offer__text">
                            Оказывает любые виды услуг по программам.<br/>
                            Также мы предоставляем полный комплекс услуг по обновлению<br/>
                            и поддержке программных продуктов на платформе 1С.
                        </div>
                        <a href="/offers" class="btn btn-secondary-faded offer__btn">Акция</a>
                    </div>
                </div>
                <div class="offer__bg-image mr-n130">
                    <img src="/images/man.png">
                </div>
            </div>
        </div>
    </div>
    <div class="mb-30"></div>
    <div class="offer bg-primary text-white">
        <div class="container position-relative">
            <div class="row align-items-center">
                <div class="col-12 col-md-8 text-center text-md-left">
                    <div class="offer__content">
                        <div class="h1 offer__title">
                            <span class="text-secondary-faded">20% скидка</span><br/>
                            на обслуживание в мае!
                        </div>
                        <div class="offer__text">
                            Оказывает любые виды услуг по программам.<br/>
                            Также мы предоставляем полный комплекс услуг по обновлению<br/>
                            и поддержке программных продуктов на платформе 1С.
                        </div>
                        <a href="/offers" class="btn btn-secondary-faded offer__btn">Акция</a>
                    </div>
                </div>
                <div class="offer__bg-image mr-n130">
                    <img src="/images/man.png">
                </div>
            </div>
        </div>
    </div>
    @include('sections.we-will-call')
@endsection