@extends('layout')
@section('title', "Главная")
@section('body')
    <section class="section-header" style="background-image: url('./images/bg-office-work.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-6 text-white header-slider">
                    <div class="glide offers-slider" data-glide-perview="1">
                        <div class="glide__track" data-glide-el="track">
                            <ul class="glide__slides">
                                <li class="glide__slide">
                                    <h1 class="mb-20">Официальный франчайзи 1С</h1>
                                    <div class="mb-md-130">
                                        Оказывает любые виды услуг по программам.<br class="d-none d-lg-block"/>
                                        Также мы предоставляем полный комплекс услуг<br class="d-none d-lg-block"/>
                                        по обновлению и поддержке программных продуктов на платформе 1С.
                                    </div>
                                </li>
                            </ul>
                            <div data-glide-el="controls" class="text-center text-md-left mb-30 mb-md-0">
                                <button data-glide-dir="<"
                                        class="btn btn-sm mr-2 btn-outline-secondary-faded btn-circle">
                                    <i class="icon-arrow-left"></i>
                                </button>
                                <button data-glide-dir=">" class="btn btn-sm btn-outline-secondary-faded btn-circle">
                                    <i class="icon-arrow-right"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 offset-md-1">
                    <form class="form-validation" action="/test" method="post" novalidate>
                        <div class="form__content">
                            <div class="input-validation w-100 mb-3">
                                <input class="form-control mb-4 text-white" placeholder="Имя" required>
                                <i class="input-validation__icon"></i>
                            </div>
                            <div class="input-validation w-100 mb-3">
                                <input class="form-control mb-4 text-white" placeholder="Номер телефона" type="tel"
                                       required>
                                <i class="input-validation__icon"></i>
                            </div>
                            <div class="custom-control custom-checkbox checkbox-terms mb-4">
                                <input type="checkbox" checked class="custom-control-input" id="header-cb-terms-1"
                                       required>
                                <label class="custom-control-label " for="header-cb-terms-1">
                                    Я соглашаюсь с
                                    <a href="/terms" class="text-secondary-faded" target="_blank">политикой
                                        конфиденциальности</a>
                                </label>
                            </div>
                            <button class="btn btn-sm btn-secondary-faded" type="submit">Заказать звонок</button>
                        </div>
                        <div class="form__success-message text-white">
                            <div class="h2 text-secondary-faded">Спасибо за обращение!</div>
                            <div>Ваша заявка принята и скоро мы с вами свяжемся!</div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <section class="py-4">
        <div class="container">
            <div class="glide glide--hover-fix" data-glide-perview="1" data-glide-gap="400">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
                        <li class="glide__slide">
                            <div class="row no-gutters">
                                <div class="col-12 col-md-4">
                                    <div class="card card-service bg-secondary-faded text-white text-left">
                                        <div class="card-body align-items-start">
                                            <small class="font-weight-bold mb-3">Готовые решения</small>
                                            <div class="h2 card-title mb-3">Продукты</div>
                                            <div class="mb-3 font-weight-light">
                                                Отслеживание трат
                                                Отслеживание задолженности
                                                Следование плану
                                                Сравнение показателей
                                                Управление бизнесом
                                            </div>
                                            <button class="btn btn-dark btn-sm position-static">Подробнее</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="card card-service bg-primary text-white text-left">
                                        <div class="card-body align-items-start">
                                            <small class="font-weight-bold mb-3">Готовые решения</small>
                                            <div class="h2 card-title mb-3">Услуги</div>
                                            <div class="mb-3 font-weight-light">
                                                Отслеживание трат
                                                Отслеживание задолженности
                                                Следование плану
                                                Сравнение показателей
                                                Управление бизнесом
                                            </div>
                                            <button class="btn btn-light btn-sm position-static">Подробнее</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 col-md-4">
                                    <div class="card card-service bg-secondary-faded text-white text-left">
                                        <div class="card-body align-items-start">
                                            <small class="font-weight-bold mb-3">Готовые решения</small>
                                            <div class="card-title mb-3">Отрасли</div>
                                            <div class="mb-3 font-weight-light">
                                                Отслеживание трат
                                                Отслеживание задолженности
                                                Следование плану
                                                Сравнение показателей
                                                Управление бизнесом
                                            </div>
                                            <button class="btn btn-dark btn-sm position-static">Подробнее</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="glide__arrows" data-glide-el="controls">
                    <button data-glide-dir="<"
                            class="btn btn-sm mr-2 btn-outline-secondary-faded btn-circle glide__arrow glide__arrow--left">
                        <i class="icon-arrow-left"></i>
                    </button>
                    <button data-glide-dir=">"
                            class="btn btn-sm btn-outline-secondary-faded btn-circle glide__arrow glide__arrow--right">
                        <i class="icon-arrow-right"></i>
                    </button>
                </div>
            </div>
    </section>
    @include('sections.offers-slider')
    @include('sections.who-are-we')
    @include('sections.international-work')
    @include('sections.state-companies')
    @include('sections.achievements')
    @include('sections.who-needs-us')
    @include('sections.you-will-get')
    @include('sections.our-team')
    @include('sections.93-divider')
    @include('sections.our-projects')
    @include('sections.video-reviews')
    @include('sections.text-reviews')
    @include('sections.usefull-articles')
    @include('sections.we-will-call')
@endsection