@extends('layout')
@section('title', "Отзывы")
@section('body')
    <div class="container">
        <div class="page-tree">
            <a href="#" class="page-tree__item">Главная</a>
            <a class="page-tree__item">Отзывы</a>
        </div>
    </div>
    <section class="py-0">
        <div class="container">
            <div class="d-flex text-primary align-items-center mb-70">
                <h1>Видеообзор</h1>
            </div>
            <div class="glide" data-glide-perview="1">
                <div class="glide__track" data-glide-el="track">
                    <ul class="glide__slides">
                        <li class="glide__slide">
                            <div class="row">
                                <div class="col-12 col-md-6 mb-30">
                                    <div class="video-review-mini">
                                        <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-30">
                                    <div class="video-review-mini">
                                        <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-30">
                                    <div class="video-review-mini">
                                        <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-30">
                                    <div class="video-review-mini">
                                        <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-30">
                                    <div class="video-review-mini">
                                        <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-30">
                                    <div class="video-review-mini">
                                        <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-30 mb-md-0">
                                    <div class="video-review-mini">
                                        <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                        </button>
                                    </div>
                                </div>
                                <div class="col-12 col-md-6 mb-30 mb-md-0">
                                    <div class="video-review-mini">
                                        <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="mx-auto d-none d-md-block mt-40">
                    <div class="col text-center" data-glide-el="controls">
                        <button data-glide-dir="<" class="btn btn-sm btn-outline-secondary-faded btn-circle">
                            <i class="icon-arrow-left"></i>
                        </button>
                        <span class="slider-indices mx-2 text-primary font-montserrat font-weight-medium">1/5</span>
                        <button data-glide-dir=">" class="btn btn-sm btn-outline-secondary-faded btn-circle">
                            <i class="icon-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('sections.text-reviews')
    <section class="py-0">
        <div class="container px-60">
            <div class="col-12 col-md-10 offset-md-1">
                <div class="glide" data-glide-perview="2" data-glide-gap="45">
                    <div class="glide__track glide__track-allow-shadow mb-60" data-glide-el="track">
                        <ul class="glide__slides" style="overflow: visible">
                            <li class="glide__slide">
                                <img class="shadow" src="./images/blagodarnost.png">
                            </li>
                            <li class="glide__slide">
                                <img class="shadow" src="./images/blagodarnost.png">
                            </li>
                        </ul>
                    </div>
                    <div data-glide-el="controls" class="text-center">
                        <button data-glide-dir="<"
                                class="btn btn-sm btn-outline-secondary-faded btn-circle mr-2">
                            <i class="icon-arrow-left"></i>
                        </button>
                        <button data-glide-dir=">"
                                class="btn btn-sm btn-outline-secondary-faded btn-circle">
                            <i class="icon-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('sections.you-will-get')
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
@endsection