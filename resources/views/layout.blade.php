<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/css/jquery.fancybox.min.css" type="text/css"/>
    <link href="/css/glide.core.min.css" rel="stylesheet" type="text/css">
    <link href="/css/glide.theme.min.css" rel="stylesheet" type="text/css">
    <link href="/css/icomoon.css" rel="stylesheet" type="text/css">
    <link href="/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="/css/app.css" rel="stylesheet" type="text/css">
</head>
<body>
<header>
    <nav class="nav navbar navbar-expand-lg">
        <div class="container">
            <div class="nav__top-menu">
                <div class="navbar-brand">
                    <a href="/">
                        <img src="./images/nav-logo.svg">
                    </a>
                </div>
                <div class="d-none d-lg-flex w-100">
                    <div class="nav-item mr-auto">
                        <a href="/contacts">Контакты</a>
                    </div>
                    <div class="px-40 nav__address d-none d-xl-block mb-auto border-right">
                        г. Москва, м. Электрозаводская,
                    </div>
                    <div class="text-right">
                        <div class="mb-1 px-40 border-right">
                            <a href="mailto:info@programs93.ru">info@programs93.ru</a>
                        </div>
                        <div class="pr-40">
                            <a href="#"><i class="icon-call"></i></a>
                            <a href="#"><i class="icon-telegram"></i></a>
                            <a href="#"><i class="icon-whatsapp"></i></a>
                        </div>
                    </div>
                    <div class="pl-40">
                        <div class="mb-1">
                            <a class="nav__phone" href="tel:+7 (499) 455-09-91">+7 (499)
                                <span class="text-secondary-faded">455-09-91</span></a>
                        </div>
                        <div>
                            <a class="nav__phone" href="tel:+7 (499) 455-09-91">+7 (499)
                                <span class="text-secondary-faded">455-09-91</span></a>
                        </div>
                    </div>
                </div>
                <button class="navbar-toggler ml-auto px-20" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent"
                        aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="text-secondary icon-hamburger"></i>
                </button>
            </div>
        </div>
        <div class="collapse navbar-collapse w-100" id="navbarSupportedContent">
            <div class="container nav__bottom-menu d-flex flex-column flex-lg-row justify-content-between align-items-center">
                <div class="nav-item my-3 my-lg-0">
                    <a href="#" class="d-lg-none">Программы</a>
                    <div class="dropdown d-none d-lg-block">
                        <a href="#" data-toggle="dropdown">Программы</a>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Бухгалтерский и налоговый учёт</a>
                            <a class="dropdown-item" href="#">Документооборот</a>
                            <a class="dropdown-item" href="#">Комплексная автоматизация</a>
                            <div class="dropdown dropright">
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Бухгалтерский и налоговый учёт</a>
                                    <a class="dropdown-item" href="#">Документооборот</a>
                                    <a class="dropdown-item" href="#">Комплексная автоматизация</a>
                                    <div class="dropdown dropright">
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Бухгалтерский и налоговый учёт</a>
                                            <a class="dropdown-item" href="#">Документооборот</a>
                                            <a class="dropdown-item" href="#">Комплексная автоматизация</a>
                                            <a class="dropdown-item" href="#">Лицензии и совместные продукты</a>
                                            <a class="dropdown-item" href="#">Реальная автоматизация</a>
                                        </div>
                                        <a class="dropdown-item" href="#">Лицензии и совместные продукты</a>
                                    </div>
                                    <a class="dropdown-item" href="#">Складской учет и логистика</a>
                                    <a class="dropdown-item" href="#">Кадровый учет</a>
                                    <a class="dropdown-item" href="#">Управленческий учёт и МСФО</a>
                                    <div class="dropdown dropright">
                                        <div class="dropdown-menu">
                                            <a class="dropdown-item" href="#">Реальная автоматизация</a>
                                            <a class="dropdown-item" href="#">Складской учет и логистика</a>
                                            <a class="dropdown-item" href="#">Кадровый учет</a>
                                        </div>
                                        <a class="dropdown-item" href="#">Сервисы 1С</a>
                                    </div>
                                    <a class="dropdown-item" href="#">Торговый учёт и продажи</a>
                                </div>
                                <a class="dropdown-item" href="#">Лицензии и совместные продукты</a>
                            </div>
                            <a class="dropdown-item" href="#">Реальная автоматизация</a>
                            <a class="dropdown-item" href="#">Складской учет и логистика</a>
                            <a class="dropdown-item" href="#">Кадровый учет</a>
                            <div class="dropdown dropright">
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="#">Управленческий учёт и МСФО</a>
                                    <a class="dropdown-item" href="#">Сервисы 1С</a>
                                    <a class="dropdown-item" href="#">Торговый учёт и продажи</a>
                                    <a class="dropdown-item" href="#">Бухгалтерский и налоговый учёт</a>
                                    <a class="dropdown-item" href="#">Документооборот</a>
                                    <a class="dropdown-item" href="#">Комплексная автоматизация</a>
                                    <a class="dropdown-item" href="#">Лицензии и совместные продукты</a>
                                    <a class="dropdown-item" href="#">Реальная автоматизация</a>
                                    <a class="dropdown-item" href="#">Складской учет и логистика</a>
                                    <a class="dropdown-item" href="#">Кадровый учет</a>
                                </div>
                                <a class="dropdown-item" href="#">Управленческий учёт и МСФО</a>
                            </div>
                            <a class="dropdown-item" href="#">Сервисы 1С</a>
                            <a class="dropdown-item" href="#">Торговый учёт и продажи</a>
                        </div>
                    </div>
                </div>
                <div class="nav-item mb-3 mb-lg-0">
                    <a href="/services">Услуги</a>
                </div>
                <div class="nav-item mb-3 mb-lg-0">
                    <a href="/industries">Отрасли</a>
                </div>
                <div class="nav-item mb-3 mb-lg-0">
                    <a href="/offers">Акции</a>
                </div>
                <div class="nav-item mb-3 mb-lg-0">
                    <a href="/projects">Проекты</a>
                </div>
                <div class="nav-item mb-3 mb-lg-0">
                    <a href="/price">Прайс</a>
                </div>
                <div class="nav-item mb-3 mb-lg-0">
                    <a href="/about">О нас</a>
                </div>
                <div class="nav-item mb-3 mb-lg-0">
                    <a href="/blog">Блог</a>
                </div>
                <div class="nav-item mb-3 mb-lg-0 ml-lg-20">
                    <div class="input-icon">
                        <input class="form-control" type="search">
                        <i class="input-icon__icon icon-search"></i>
                    </div>
                </div>
                <div class="nav-item">
                    <button class="btn btn-secondary-faded"
                            data-toggle="modal" data-target="#call-modal">Заказать звонок
                    </button>
                </div>
            </div>
        </div>
    </nav>
</header>
@yield('body')
<div class="modal fade" id="call-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg justify-content-center" role="document">
        <form class="form-validation text-center" novalidate>
            <div class="modal-content">
                <div class="form__content w-100">
                    <div class="modal-header">
                        <h2 class="modal-title text-primary">Мы <span
                                    class="text-secondary-faded">с радостью</span> свяжемся с вами!</h2>
                        <div>Если у вас остались вопросы, вы можете оставить
                            номер <br class="d-none d-lg-inline"/>телефона и мы с вами
                            свяжемся
                        </div>
                    </div>
                    <div class="modal-body">
                        <div class="input-validation w-100 mb-3">
                            <input type="text" class="form-control" placeholder="Имя" required>
                            <i class="input-validation__icon"></i>
                        </div>
                        <div class="input-validation w-100 mb-3">
                            <input class="form-control" type="email" placeholder="Email" required>
                            <i class="input-validation__icon"></i>
                        </div>
                        <div class="input-validation w-100 mb-40">
                            <input class="form-control" type="tel" placeholder="Номер телефона" required>
                            <i class="input-validation__icon"></i>
                        </div>
                        <div class="custom-control custom-checkbox checkbox-terms mb-40">
                            <input type="checkbox" class="custom-control-input" id="cb-terms-1" required>
                            <label class="custom-control-label" for="cb-terms-1">
                                Я соглашаюсь с
                                <a href="/terms" class="text-secondary-faded" target="_blank">политикой
                                    конфиденциальности</a>
                            </label>
                        </div>
                        <button class="btn btn-secondary-faded" type="submit"
                                data-toggle="modal" data-target="#modal-1">Заказать звонок
                        </button>
                    </div>
                </div>
                <div class="form__success-message"><h2 class="modal-title text-primary">Спасибо за обращение!</h2>
                    <div>
                        Ваша заявка принята и скоро мы с вами свяжемся!
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-4 col-lg-2 mb-4 mb-md-0">
                <h4 class="mb-20 text-secondary-faded">О компании</h4>
                <ul class="list-unstyled">
                    <li><a class="w-100" href="/">Главная</a></li>
                    <li><a class="w-100" href="/about">О компании</a></li>
                    <li><a class="w-100" href="/price">Прайс лист</a></li>
                    <li><a class="w-100" href="/projects">Проекты</a></li>
                    <li><a class="w-100" href="/news">Новости</a></li>
                    <li><a class="w-100" href="/contacts">Контакты</a></li>
                    <li><a class="w-100" href="/offers">Акции</a></li>
                    <li><a class="w-100" href="/clients">Клиенты</a></li>
                    <li><a class="w-100" href="/blog">Блог</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-3 mb-4 mb-md-0">
                <h4 class="mb-20 text-secondary-faded">Продукты</h4>
                <ul class="list-unstyled">
                    <li><a class="w-100" href="#">1С: Бухгалтерия</a></li>
                    <li><a class="w-100" href="#">1С: Зарплата и Управление Персоналом</a></li>
                    <li><a class="w-100" href="#">1С: Управление Торговлей</a></li>
                    <li><a class="w-100" href="#">1С: Управление Производственным Предприятием</a></li>
                    <li><a class="w-100" href="#">1С: Консолидация</a></li>
                    <li><a class="w-100" href="#">1С: Управление Производственным Предприятием</a></li>
                    <li><a class="w-100" href="#">1С: Консолидация</a></li>
                </ul>
            </div>
            <div class="col-12 col-md-4 col-lg-4 mb-4 mb-md-0">
                <h4 class="mb-20 text-secondary-faded">Услуги</h4>
                <ul class="list-unstyled">
                    <li><a class="w-100" href="#">Установка 1С</a></li>
                    <li><a class="w-100" href="#">Поддержка 1С</a></li>
                    <li><a class="w-100" href="#">Постановка управленческого и финансового учета</a></li>
                    <li><a class="w-100" href="#">Программирование 1С</a></li>
                    <li><a class="w-100" href="#">Оптимизация работы систем 1С</a></li>
                    <li><a class="w-100" href="#">Продажа программных продуктов 1С</a></li>
                    <li><a class="w-100" href="#">Консалтинговые услуги</a></li>
                    <li><a class="w-100" href="#">Курсы 1С:Бухгалтерия и 1С</a></li>
                    <li><a class="w-100" href="#">Предприятие</a></li>
                    <li><a class="w-100" href="#">Обучение 1С</a></li>
                    <li><a class="w-100" href="#">1С: ИТС</a></li>
                </ul>
            </div>
            <div class="col-12 col-lg-3 text-left text-lg-right d-flex flex-column">
                <div class="mb-30">
                    <a href="tel:89998402030" class="footer__phone">
                        +7 (499) <span class="text-secondary-faded">455-09-91</span>
                    </a>
                </div>
                <div class="mb-30">
                    <button class="btn btn-secondary-faded btn-sm" data-toggle="modal"
                            data-target="#call-modal">Заказать звонок
                    </button>
                </div>
                <div class="mb-30 font-montserrat" style="letter-spacing: -0.43px;">
                    115280, г. Москва,<br/>
                    м. Электрозаводская
                </div>
                <a href="mailto:info@programs93.ru" class="font-weight-normal">info@programs93.ru</a>
                <div class="mt-auto text-white">
                    <a class="pr-3" href="/blog"><i class="icon-phone"></i></a>
                    <a class="text-white pr-3" href="/blog"><i class="icon-telegram"></i></a>
                    <a class="text-white" href="/blog"><i class="icon-whatsapp"></i></a>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="/js/jquery-3.4.0.slim.min.js"></script>
<script src="/js/jquery.fancybox.min.js"></script>
<script src="/js/glide.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/app.js"></script>
</body>
</html>
