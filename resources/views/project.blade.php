@extends('layout')
@section('title', "Проект")
@section('body')
    <div class="container page-tree page-tree--absolute">
        <div>
            <a href="#" class="page-tree__item">Главная</a>
            <a href="#" class="page-tree__item">Проекты</a>
            <a class="page-tree__item">Интеграция</a>
        </div>
    </div>
    <section class="section-header" style="background-image: url('./images/bg-office-work.jpg')">
        <div class="container">
            <div class="project">
                <div class="project__header">
                    <div class="project-header__date">
                        <div class="h1">21</div>
                        <div>Января 2019</div>
                    </div>
                    <div class="project-header__title">
                        <div class="h2">Интеграция</div>
                        <div>ОАО "Воентелеком"</div>
                    </div>
                    <div class="project-header__stars ml-auto">
                        <img src="./images/star.svg">
                        <img src="./images/star.svg">
                        <img src="./images/star.svg">
                        <img src="./images/star.svg">
                        <img src="./images/star.svg">
                    </div>
                </div>
                <div class="project-body">
                    <div class="row">
                        <div class="col-12 col-md-6">
                            <div class="h3 project-body__header">
                                Нужны ли услуги программиста 1С? Разумеется, нет! Кому
                                нужны отлаженные бизнес-процессы</div>
                            <div class="project-body__text">
                                Уверенность в стабильной работе программы и
                                индивидуальные&nbsp;решения для бизнеса, которые сэкономят&nbsp;деньги
                                и рабочее время
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="pb-200 pt-90">
        <div class="container">
            <div class="mb-90 bg-93-top-right">
                <h3 class="h2 mb-2">Информация о проекте</h3>
                <div class="row">
                    <div class="col-12 col-md-8 text-gray">
                        Автоматизация на основе 1C:ERP, в отличие от отдельных функциональных
                        решений, связывает работу всех подразделений. Она позволяет построить комплексную информационную
                        систему управления предприятием, повысить прозрачность бизнес-процессов и обеспечить принятие
                        эффективных управленческих решений.
                    </div>
                </div>
            </div>
            <div class="mb-90">
                <h3 class="h2 mb-2">Цели и задачи</h3>
                <div class="row">
                    <div class="col-12 col-md-8 text-gray">
                        Автоматизация на основе 1C:ERP, в отличие от отдельных функциональных
                        решений, связывает работу всех подразделений. Она позволяет построить комплексную информационную
                        систему управления предприятием, повысить прозрачность бизнес-процессов и обеспечить принятие
                        эффективных управленческих решений.
                    </div>
                </div>
            </div>
            <div class="mb-90">
                <h3 class="h2 mb-2">Как проходила работа</h3>
                {{--todo font sise 18 px or 16px?--}}
                <div class="row">
                    <div class="col-12 col-md-8 text-gray">
                        Автоматизация на основе 1C:ERP, в отличие от отдельных функциональных
                        решений, связывает работу всех подразделений. Она позволяет построить комплексную информационную
                        систему управления предприятием, повысить прозрачность бизнес-процессов и обеспечить принятие
                        эффективных управленческих решений.
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 mb-80 pr-md-40">
                    <h4 class="h3 mb-2">Доработка функций</h4>
                    <div class="mb-4 text-gray">
                        Автоматизация на основе 1C:ERP, в отличие от отдельных функциональных решений, связывает работу
                        всех подразделений. Она позволяет построить комплексную информационную систему управления
                        предприятием, повысить прозрачность бизнес-процессов и обеспечить принятие эффективных
                        управленческих решений.
                    </div>
                    <div>
                        <img class="w-100" src="./images/project-step.png">
                    </div>
                </div>
                <div class="col-12 col-md-6 mb-80 pl-md-40">
                    <h4 class="h3 mb-2">Доработка функций</h4>
                    <div class="mb-4 text-gray">
                        Автоматизация на основе 1C:ERP, в отличие от отдельных функциональных решений, связывает работу
                        всех подразделений. Она позволяет построить комплексную информационную систему управления
                        предприятием, повысить прозрачность бизнес-процессов и обеспечить принятие эффективных
                        управленческих решений.
                    </div>
                    <div>
                        <img class="w-100" src="./images/project-step.png">
                    </div>
                </div>
                <div class="col-12 col-md-6 mb-80 mb-md-0 pr-md-40">
                    <h4 class="h3 mb-2">Доработка функций</h4>
                    <div class="mb-4 text-gray">
                        Автоматизация на основе 1C:ERP, в отличие от отдельных функциональных решений, связывает работу
                        всех подразделений. Она позволяет построить комплексную информационную систему управления
                        предприятием, повысить прозрачность бизнес-процессов и обеспечить принятие эффективных
                        управленческих решений.
                    </div>
                    <div>
                        <img class="w-100" src="./images/project-step.png">
                    </div>
                </div>
                <div class="col-12 col-md-6 mb-80 mb-md-0 pl-md-40">
                    <h4 class="h3 mb-2">Доработка функций</h4>
                    <div class="mb-4 text-gray">
                        Автоматизация на основе 1C:ERP, в отличие от отдельных функциональных решений, связывает работу
                        всех подразделений. Она позволяет построить комплексную информационную систему управления
                        предприятием, повысить прозрачность бизнес-процессов и обеспечить принятие эффективных
                        управленческих решений.
                    </div>
                    <div>
                        <img class="w-100" src="./images/project-step.png">
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('sections.video-reviews')
    <section class="pt-200 pb-130 section-you-get text-primary">
        <div class="container">
            <h2 class="h1 mb-60 font-weight-bold">Как улучшились показатели</h2>
            <div class="row font-montserrat">
                <div class="col-12 col-md-5">
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar"
                             style="width: 40%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">40%
                        </div>
                    </div>
                    <div class="mb-40">Экономия времени</div>
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar"
                             style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">90%
                        </div>
                    </div>
                    <div class="mb-40">Эффективность работы сотрудников</div>
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar"
                             style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%
                        </div>
                    </div>
                    <div class="mb-40">Выше производительность труда</div>
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar"
                             style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%
                        </div>
                    </div>
                    <div>Повышение производительности компании</div>
                </div>
                <div class="col-12 col-md-5 offset-md-1">
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar"
                             style="width: 40%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">40%
                        </div>
                    </div>
                    <div class="mb-40">Экономия времени</div>
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar"
                             style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">90%
                        </div>
                    </div>
                    <div class="mb-40">Эффективность работы сотрудников</div>
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar"
                             style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%
                        </div>
                    </div>
                    <div class="mb-40">Выше производительность труда</div>
                    <div class="progress mb-3">
                        <div class="progress-bar" role="progressbar"
                             style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%
                        </div>
                    </div>
                    <div>Повышение производительности компании</div>
                </div>
            </div>
        </div>
    </section>
    <div class="mt-n200"></div>
    @include('sections.advantages')
    @include('sections.achievements')
    @include('sections.text-reviews')
    <div class="mt-n200"></div>
    @include('sections.we-will-call')
@endsection