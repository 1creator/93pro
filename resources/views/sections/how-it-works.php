<section class="pt-80">
    <div class="container">
        <h2 class="mb-20">Как работает?</h2>
        <div class="text-gray mb-70 font-weight-light">
            Для решения задач поддержки программных продуктов и информационных систем в актуальном состоянии, центр
            сопровождения «1С-Рарус ИТС» предлагает услугу «Информационно-Технологическое Сопровождение для
            пользователей 1С:Предприятие» (1С:ИТС). Данная услуга обеспечивает пользователям программ 1С:Предприятие
            своевременное обновление конфигураций, дает право на получение методических рекомендаций и материалов по
            работе с программой, гарантирует полное соответствие учетной системы действующему налоговому
            законодательству.
        </div>
        <div class="row no-gutters">
            <div class="col-12 col-md-4">
                <div class="bg-secondary-faded h-100 p-40 d-flex align-items-center text-center">
                    <div class="h3 px-20">
                        Своевременное
                        обновление
                        конфигураций
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="bg-primary p-40 h-100 d-flex align-items-center text-white text-center">
                    <div class="h3 px-20">
                        Облегчение
                        работы
                        бухгалтерии
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="bg-secondary-faded h-100 p-40 d-flex align-items-center text-center">
                    <div class="h3 px-40">
                        Улучшение
                        качества
                        работы
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>