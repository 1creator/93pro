<section class="py-200">
    <div class="container">
        <h2 class="h1">Стоимость договора ИТС</h2>
        <table class="table table-borderless mb-5">
            <tbody>
            <tr>
                <td>
                    <div class="h3 text-primary text-left">ИТС ПРОФ Комфортный</div>
                </td>
                <td>
                    <div class="text-nowrap text-left">1 месяц</div>
                </td>
                <td>
                    <div class="font-weight-bold">от 1 000р.</div>
                </td>
                <td class="text-right">
                    <button class="btn btn-secondary-faded px-50"
                            data-toggle="modal" data-target="#service-modal-1">Выбрать
                    </button>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="h3 text-primary text-left">ИТС ПРОФ Комфортный</div>
                </td>
                <td>
                    <div class="text-nowrap text-left">3 месяца</div>
                </td>
                <td>
                    <div class="font-weight-bold">от 1 000р.</div>
                </td>
                <td class="text-right">
                    <button class="btn btn-secondary-faded px-50"
                            data-toggle="modal" data-target="#service-modal-1">Выбрать
                    </button>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="h3 text-primary text-left">ИТС ПРОФ Комфортный</div>
                </td>
                <td>
                    <div class="text-nowrap text-left">5 месяцев</div>
                </td>
                <td>
                    <div class="font-weight-bold">от 1 000р.</div>
                </td>
                <td class="text-right">
                    <button class="btn btn-secondary-faded px-50"
                            data-toggle="modal" data-target="#service-modal-1">Выбрать
                    </button>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="h3 text-primary text-left">ИТС ПРОФ Комфортный</div>
                </td>
                <td>
                    <div class="text-nowrap text-left">24 месяца</div>
                </td>
                <td>
                    <div class="font-weight-bold">от 1 000р.</div>
                </td>
                <td class="text-right">
                    <button class="btn btn-secondary-faded px-50"
                            data-toggle="modal" data-target="#service-modal-1">Выбрать
                    </button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="modal fade" id="service-modal-1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <form action="/test" method="post" class="form-validation" novalidate>
                    <div class="form__content">
                        <div class="modal-header">
                            <div class="h2 modal-title text-primary mb-3">ИТС ПРОФ Комфортный</div>
                            <div class="font-weight-bold" style="font-size: 24px">
                                5 месяцев <strong>15 000 р.</strong>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="input-validation w-100 mb-3">
                                <input class="form-control" type="text" placeholder="Имя" required>
                                <i class="input-validation__icon"></i>
                            </div>
                            <div class="input-validation w-100 mb-3">
                                <input class="form-control" type="email" placeholder="Email" required>
                                <i class="input-validation__icon"></i>
                            </div>
                            <div class="input-validation w-100 mb-40">
                                <input class="form-control" type="tel" placeholder="Номер телефона" required>
                                <i class="input-validation__icon"></i>
                            </div>
                            <div class="custom-control custom-checkbox checkbox-terms mb-40">
                                <input type="checkbox" class="custom-control-input" id="cb-terms-3" required>
                                <label class="custom-control-label" for="cb-terms-3">
                                    Я соглашаюсь с
                                    <a href="/terms" class="text-secondary-faded" target="_blank">политикой
                                        конфиденциальности</a>
                                </label>
                            </div>
                            <button class="btn btn-secondary-faded">Заказать звонок</button>
                        </div>
                    </div>
                    <div class="form__success-message text-center">
                        <div class="h2 modal-title text-primary">Спасибо за обращение!</div>
                        <div>
                            Ваша заявка принята и скоро мы с вами свяжемся!
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>