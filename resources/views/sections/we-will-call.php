<section class="py-200">
    <div class="container text-primary">
        <form class="form-validation" novalidate>
            <div class="form__content">
                <div class="h1 mb-20">Мы <span class="text-secondary-faded">с радостью</span> свяжемся с вами!</div>
                <div class="row mb-50">
                    <div class="col-12 col-md-6">
                        Если у вас остались вопросы, вы можете оставить номер телефона и мы с вами свяжемся
                    </div>
                </div>
                <div class="row mb-50">
                    <div class="col-12 col-md-5 mb-4 mb-md-0">
                        <div class="input-validation">
                            <input class="form-control" placeholder="Имя" required>
                            <i class="input-validation__icon"></i>
                        </div>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="input-validation">
                            <input class="form-control" placeholder="Номер телефона" required>
                            <i class="input-validation__icon"></i>
                        </div>
                    </div>
                </div>
                <div class="row align-items-center">
                    <div class="col-12 col-md-5 mb-4 mb-md-0">
                        <button class="btn btn-secondary-faded px-40">Заказать звонок</button>
                    </div>
                    <div class="col-12 col-md-5">
                        <div class="custom-control custom-checkbox checkbox-terms">
                            <input type="checkbox" checked class="custom-control-input" id="we-will-call-cb-terms"
                                   required>
                            <label class="custom-control-label " for="we-will-call-cb-terms">
                                Я соглашаюсь с
                                <a href="/terms" class="text-secondary-faded" target="_blank">политикой
                                    конфиденциальности</a>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form__success-message">
                <div class="h2 modal-title text-primary">Спасибо за обращение!</div>
                <div>
                    Ваша заявка принята и скоро мы с вами свяжемся!
                </div>
            </div>
        </form>
    </div>
</section>