<section class="pt-200 pb-130 section-you-get text-primary">
    <div class="container">
        <h3 class="h1 mb-60 font-weight-bold">С нами у вас будет:</h3>
        <div class="row">
            <div class="col-12 col-md-5 font-montserrat">
                <div class="progress mb-3">
                    <div class="progress-bar" role="progressbar"
                         style="width: 40%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">40%
                    </div>
                </div>
                <div class="mb-40">Экономия времени</div>
                <div class="progress mb-3">
                    <div class="progress-bar" role="progressbar"
                         style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">90%
                    </div>
                </div>
                <div class="mb-40">Эффективность работы сотрудников</div>
                <div class="progress mb-3">
                    <div class="progress-bar" role="progressbar"
                         style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%
                    </div>
                </div>
                <div class="mb-40">Выше производительность труда</div>
                <div class="progress mb-3">
                    <div class="progress-bar" role="progressbar"
                         style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%
                    </div>
                </div>
                <div>Повышение производительности компании</div>
            </div>
            <div class="col-12 col-md-7 d-none d-md-block">
                <div class="section-you-get__img-wrap">
                    <img src="./images/monitor.png">
                </div>
            </div>
        </div>
    </div>
</section>