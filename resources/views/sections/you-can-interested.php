<section class="py-0 section-you-interested">
    <div class="container">
        <h1 class="mb-4">Вас может заинтересовать</h1>
        <div class="row mb-80">
            <div class="col-12 col-lg-9 font-weight-light">
                Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы предоставляем
                полный
                комплекс услуг по обновлению и поддержке программных продуктов на платформе 1С:Предприятие 7.7.
            </div>
        </div>
        <div class="glide offers-slider glide--hover-fix"
             data-glide-perview="1" data-glide-gap="400">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                    <li class="glide__slide">
                        <div class="row no-gutters section-you-interested__cards">
                            <div class="col-12 col-md-4">
                                <div class="card card-service bg-white text-left">
                                    <div class="card-body align-items-start">
                                        <div class="h2 card-title mb-3">Поддержка</div>
                                        <div class="mb-3 font-weight-light">
                                            Отслеживание трат
                                            Отслеживание задолженности
                                            Следование плану
                                            Сравнение показателей
                                            Управление бизнесом
                                        </div>
                                        <button class="btn btn-dark btn-sm position-static">Подробнее</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="card card-service bg-white text-left">
                                    <div class="card-body align-items-start">
                                        <div class="h2 card-title mb-3">Поддержка</div>
                                        <div class="mb-3 font-weight-light">
                                            Отслеживание трат
                                            Отслеживание задолженности
                                            Следование плану
                                            Сравнение показателей
                                            Управление бизнесом
                                        </div>
                                        <button class="btn btn-dark btn-sm position-static">Подробнее</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="card card-service bg-white text-left">
                                    <div class="card-body align-items-start">
                                        <div class="h2 card-title mb-3">Поддержка</div>
                                        <div class="mb-3 font-weight-light">
                                            Отслеживание трат
                                            Отслеживание задолженности
                                            Следование плану
                                            Сравнение показателей
                                            Управление бизнесом
                                        </div>
                                        <button class="btn btn-dark btn-sm position-static">Подробнее</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="glide__arrows" data-glide-el="controls">
                <button data-glide-dir="<"
                        class="btn btn-sm mr-2 btn-outline-secondary-faded btn-circle glide__arrow glide__arrow--left">
                    <i class="icon-arrow-left"></i>
                </button>
                <button data-glide-dir=">"
                        class="btn btn-sm btn-outline-secondary-faded btn-circle glide__arrow glide__arrow--right">
                    <i class="icon-arrow-right"></i>
                </button>
            </div>
        </div>
</section>