<section class="pt-0">
    <div class="container">
        <div class="row">
            <div class="col-12 col-md-10">
                <h2 class="h1 mb-40">Программы 1С</h2>
                <div class="mb-40 font-weight-light">
                    Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы предоставляем
                    полный комплекс услуг по обновлению и поддержке программных продуктов на платформе 1С:Предприятие
                    7.7.
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-12">
                <div class="row font-weight-bold">
                    <div class="col-12 col-sm-6 col-md-4 text-center mb-60 product-preview-card">
                        <a href="/products/1">
                            <img src="/images/sample-product.png">
                            <div>Комплексная автоматизация</div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 text-center mb-60 product-preview-card">
                        <a href="/products/1">
                            <img src="/images/sample-product.png">
                            <div>Бухгалтерский и налоговый учёт</div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 text-center mb-60 product-preview-card">
                        <a href="/products/1">
                            <img src="/images/sample-product.png">
                            <div>Бухгалтерский и налоговый учёт</div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 text-center mb-60 product-preview-card">
                        <a href="/products/1">
                            <img src="/images/sample-product.png">
                            <div>Бухгалтерский и налоговый учёт</div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 text-center mb-60 product-preview-card">
                        <a href="/products/1">
                            <img src="/images/sample-product.png">
                            <div>Бухгалтерский и налоговый учёт</div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 text-center mb-60 product-preview-card">
                        <a href="/products/1">
                            <img src="/images/sample-product.png">
                            <div>Бухгалтерский и налоговый учёт</div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>