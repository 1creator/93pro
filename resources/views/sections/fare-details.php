<section class="py-200 fare-details">
    <div class="container">
        <h2 class="h1 mb-40">Что включает в себя тариф:</h2>
        <div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-1">
                    <div>Обновление 1 информационной базы</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-1" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-2">
                    <div>Обновление 3 информационных баз</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-2" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-3">
                    <div>Обновление релиза по мере выхода</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-3" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-4">
                    <div>1С-Отчетность (сервис)</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-4" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-5">
                    <div>1С:Контрагент (сервис)</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-5" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-6">
                    <div>1С-ЭДО (сервис)</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-6" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-7">
                    <div>Журнал «Бух.1C»</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-7" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-8">
                    <div>Бизнес сувенир «1C»</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-8" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-9">
                    <div>Доступ к обновлениям на сайте</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-9" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-10">
                    <div>Доступ к интернет-версии на сайте</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-10" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-11">
                    <div>Создание архивных копий баз данных</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-11" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-12">
                    <div>Диагностика баз данных</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-12" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-13">
                    <div>Бесплатная линия консультаций</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-13" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-14">
                    <div>Отправка обновлений по запросу на электронную почту</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-14" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-15">
                    <div>Приоритетное рассмотрение сообщений на Линии Консультации 1С</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-15" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
            <div class="fare-details-item">
                <a class="fare-details-item__content" data-toggle="collapse" href="#fare-details-16">
                    <div>Доступ к ответам на вопросы технической поддержки фирмы 1С</div>
                    <i class="fare-details__checkbox icon-valid"></i>
                </a>
                <div id="fare-details-16" class="collapse fare-details-item__details">
                    Оказывает любые виды услуг по программам.
                    Также мы предоставляем полный комплекс услуг по обновлению
                    и поддержке программных продуктов на платформе 1С.
                </div>
            </div>
        </div>
    </div>
</section>