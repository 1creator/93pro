<section class="pt-200 pb-0 text-primary">
    <div class="container">
        <div class="d-flex align-items-center mb-3 flex-column flex-md-row">
            <h3 class="h1 mb-30 mb-md-0" style="letter-spacing: 0.2px;">Кому и зачем мы нужны</h3>
            <a href="./about" class="btn btn-secondary-faded btn-sm ml-md-auto">
                Заказать звонок
            </a>
        </div>
        <div class="mb-60" style="font-size: 20px">
            Оказываем любые виды услуг по программам.<br class="d-none d-md-inline"/>
            Также мы предоставляем полный комплекс услуг по обновлению<br class="d-none d-md-inline"/>
            и поддержке программных продуктов на платформе 1С.
        </div>
        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="card card-client">
                    <div class="card-client__about">
                        <div class="h1">1.</div>
                        <div class="h3">Директора и руководители ИТ&nbsp;отделов</div>
                        <div>Отслеживание трат
                            Отслеживание задолженности
                            Следование плану
                            Сравнение показателей
                            Управление бизнесом
                        </div>
                    </div>
                    <div class="card card-client__services bg-secondary-faded">
                        <div class="h4">Вам может подойти</div>
                        <a href="#">Установка 1С</a>
                        <a href="#">Поддержка 1С</a>
                        <a href="#">Постановка управленческого и&nbsp;финансового учета</a>
                        <a href="#">Программирование 1С</a>
                        <a href="#">Оптимизация работы систем 1С</a>
                        <a href="#">Продажа программных продуктов 1С</a>
                        <a href="#">Консалтинговые услуги</a>
                        <a href="#">Курсы 1С:Бухгалтерия 8 и 1С:</a>
                        <a href="#">Предприятие 8</a>
                        <a href="#">Обучение 1С</a>
                        <a href="#">1С: ИТС</a>
                        <div>
                            <!--ask hover button by hover card or no-->
                            <button class="btn btn-dark btn-sm">Все услуги</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-client">
                    <div class="card-client__about">
                        <div class="h1">2.</div>
                        <div class="h3">Сисадмины - лица занимающиеся ИТ поддержкой</div>
                        <div>Отслеживание трат
                            Отслеживание задолженности
                            Следование плану
                            Сравнение показателей
                            Управление бизнесом
                        </div>
                    </div>
                    <div class="card card-client__services bg-primary">
                        <div class="h4">Вам может подойти</div>
                        <a href="#">Установка 1С</a>
                        <a href="#">Поддержка 1С</a>
                        <a href="#">Постановка управленческого и&nbsp;финансового учета</a>
                        <a href="#">Программирование 1С</a>
                        <a href="#">Оптимизация работы систем 1С</a>
                        <a href="#">Продажа программных продуктов 1С</a>
                        <a href="#">Консалтинговые услуги</a>
                        <a href="#">Курсы 1С:Бухгалтерия 8 и 1С:</a>
                        <a href="#">Предприятие 8</a>
                        <a href="#">Обучение 1С</a>
                        <a href="#">1С: ИТС</a>
                        <div>
                            <button class="btn btn-light btn-sm">Все услуги</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-client">
                    <div class="card-client__about">
                        <div class="h1">3.</div>
                        <div class="h3">Компании-подрядчики, оказывающие близкие услуги</div>
                        <div>Отслеживание трат
                            Отслеживание задолженности
                            Следование плану
                            Сравнение показателей
                            Управление бизнесом
                        </div>
                    </div>
                    <div class="card card-client__services bg-secondary-faded">
                        <div class="h4">Вам может подойти</div>
                        <a href="#">Установка 1С</a>
                        <a href="#">Поддержка 1С</a>
                        <a href="#">Постановка управленческого и&nbsp;финансового учета</a>
                        <a href="#">Программирование 1С</a>
                        <a href="#">Оптимизация работы систем 1С</a>
                        <a href="#">Продажа программных продуктов 1С</a>
                        <a href="#">Консалтинговые услуги</a>
                        <a href="#">Курсы 1С:Бухгалтерия 8 и 1С:</a>
                        <a href="#">Предприятие 8</a>
                        <a href="#">Обучение 1С</a>
                        <a href="#">1С: ИТС</a>
                        <div>
                            <button class="btn btn-dark btn-sm">Все услуги</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>