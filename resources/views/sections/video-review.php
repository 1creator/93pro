<section class="py-0">
    <div class="container">
        <div class="d-flex text-primary align-items-center mb-70 flex-column flex-md-row">
            <h3 class="mb-30 h1 mb-md-0">Видеообзор</h3>
            <a href="#" class="btn btn-secondary-faded ml-md-auto">Перейти к отзывам</a>
        </div>
        <div class="video-review-big">
            <iframe src="https://www.youtube.com/embed/HfnINdtw_8E"
                    frameborder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowfullscreen></iframe>
        </div>
    </div>
</section>