<section class="py-0">
    <div class="container">
        <div class="d-flex align-items-center text-primary mb-70 flex-column flex-md-row">
            <h3 class="h1 mb-30 mb-md-0">Полезно знать</h3>
            <a href="#" class="btn btn-secondary-faded ml-md-60 px-md-40">Все новости</a>
        </div>
        <div class="row">
            <div class="col-12 col-md-7 mb-30">
                <div class="card-article-preview bg-dark">
                    <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                    <a href="#" class="card-article-preview__caption">
                        Нужны ли услуги программиста 1С? Разумеется, нет! Кому нужны
                        отлаженные бизнес-процессы
                    </a>
                    <div class="card-article-preview__text">
                        Уверенность в стабильной работе программы и индивидуальные
                        решения для бизнеса, которые сэкономят деньги и рабочее время
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="#" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                        <div><i class="icon-eye"></i>
                            <small>2144</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-5 mb-30">
                <div class="card-article-preview bg-secondary-faded">
                    <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                    <a href="#" class="card-article-preview__caption">
                        Нужны ли услуги1С?
                        Разумеется, нет!
                    </a>
                    <div class="card-article-preview__text">
                        Уверенность в стабильной работе программы и индивидуальные
                        решения для бизнеса, которые сэкономят деньги и рабочее время
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="#" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                        <div><i class="icon-eye"></i>
                            <small>2144</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-5 mb-30 mb-md-0">
                <div class="card-article-preview bg-dark">
                    <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                    <a href="#" class="card-article-preview__caption">
                        Нужны ли услуги1С?
                        Разумеется, нет!
                    </a>
                    <div class="card-article-preview__text">
                        Уверенность в стабильной работе программы и индивидуальные
                        решения для бизнеса, которые сэкономят деньги и рабочее время
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="#" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                        <div><i class="icon-eye"></i>
                            <small>2144</small>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-7 mb-md-0">
                <div class="card-article-preview bg-dark card-article-preview--bg-img"
                     style="background-image: url('./images/bg-office-work.jpg')">
                    <div class="card-article-preview__date">12 ФЕВРАЛЯ 2017</div>
                    <a href="#" class="card-article-preview__caption">
                        Нужны ли услуги программиста 1С? Разумеется, нет! Кому нужны
                        отлаженные бизнес-процессы
                    </a>
                    <div class="card-article-preview__text">
                        Уверенность в стабильной работе программы и индивидуальные
                        решения для бизнеса, которые сэкономят деньги и рабочее время
                    </div>
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="#" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                        <div><i class="icon-eye"></i>
                            <small>2144</small>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>