<section class="calculator">
    <div class="container">
        <div class="row mb-70">
            <div class="col">
                <h2 class="h1 mb-30 text-primary">Рассчитать стоимость</h2>
                <div class="d-flex align-items-center flex-column flex-md-row">
                    <div class="mb-4 mb-md-0">
                        Получите рассчет прямо сейчас!
                    </div>
                    <button class="ml-md-30 btn btn-secondary-faded px-md-40">
                        Cмотреть прайс
                    </button>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-md-6 mb-5 mb-md-0">
                <div class="mb-5">
                    <div class="mb-3">
                        Количество пользователей 1С
                    </div>
                    <input class="w-100" type="range" min="0" max="100">
                </div>
                <div class="mb-5">
                    <div class="mb-3">
                        Количество пользователей 1С
                    </div>
                    <input class="w-100" type="range" min="0" max="100">
                </div>
                <div class="mb-5">
                    <div class="mb-3">
                        Использование конфигураций 1С
                    </div>
                    <div class="d-flex mb-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio1" name="radio" class="custom-control-input">
                            <label class="custom-control-label" for="radio1">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio ml-5">
                            <input type="radio" id="radio2" name="radio" class="custom-control-input">
                            <label class="custom-control-label" for="radio2">Установка 1С</label>
                        </div>
                    </div>
                    <div class="d-flex mb-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio3" name="radio1" class="custom-control-input">
                            <label class="custom-control-label" for="radio3">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio ml-5">
                            <input type="radio" id="radio4" name="radio1" class="custom-control-input">
                            <label class="custom-control-label" for="radio4">Установка 1С</label>
                        </div>
                    </div>
                    <div class="d-flex mb-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio5" name="radio2" class="custom-control-input">
                            <label class="custom-control-label" for="radio5">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio ml-5">
                            <input type="radio" id="radio6" name="radio2" class="custom-control-input">
                            <label class="custom-control-label" for="radio6">Установка 1С</label>
                        </div>
                    </div>
                    <div class="d-flex mb-2">
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio7" name="radio3" class="custom-control-input">
                            <label class="custom-control-label" for="radio7">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio ml-5">
                            <input type="radio" id="radio8" name="radio3" class="custom-control-input">
                            <label class="custom-control-label" for="radio8">Установка 1С</label>
                        </div>
                    </div>
                </div>
                <div class="mb-5">
                    <div class="mb-3">
                        Количество часов работы программиста
                    </div>
                    <input class="w-100" type="range" min="0" max="100">
                </div>
                <div>
                    <a class="d-block mb-3 text-decoration-none" data-toggle="collapse" href="#additionalService">
                        Дополнительные услуги
                        <i class="text-secondary icon-sm icon-caret-down"></i>
                    </a>
                    <div class="collapse" id="additionalService">
                        <div class="custom-control custom-radio mb-2">
                            <input type="radio" id="radio9" name="radio4" class="custom-control-input">
                            <label class="custom-control-label" for="radio9">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio mb-2">
                            <input type="radio" id="radio10" name="radio4" class="custom-control-input">
                            <label class="custom-control-label" for="radio10">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio mb-2">
                            <input type="radio" id="radio11" name="radio4" class="custom-control-input">
                            <label class="custom-control-label" for="radio11">Установка 1С</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input type="radio" id="radio12" name="radio4" class="custom-control-input">
                            <label class="custom-control-label" for="radio12">Установка 1С</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-6 pl-md-80">
                <div class="shadow-lg p-40">
                    <div class="mb-5">
                        <div class="h3 mb-3 text-black">СТОИМОСТЬ СОПРОВОЖДЕНИЯ</div>
                        <div style="line-height: 2rem;">
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                        </div>
                    </div>
                    <div class="mb-5">
                        <div class="h3 mb-3 text-black">ДОПОЛНИТЕЛЬНЫЕ УСЛУГИ</div>
                        <div style="line-height: 2rem;">
                            Базы для обновления<br/>
                            Базы для обновления<br/>
                        </div>
                    </div>
                    <div class="h3 text-secondary mb-5" style="font-size: 36px">~27 000 Р</div>
                    <div>
                        <button class="btn btn-dark px-4 mb-5">Оставить заявку</button>
                    </div>
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="check2">
                        <label class="custom-control-label small" for="check2">
                            Я соглашаюсь с
                            <a href="/terms" class="text-secondary-faded" target="_blank">политикой
                                конфиденциальности</a>
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
