<section class="pt-0">
    <div class="bg-primary text-white py-40">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-12 col-md-7 mb-30 mb-md-0">
                    <h3 class="h1 mb-3">Наша команда</h3>
                    <div class="font-weight-thin" style="letter-spacing: 1px;">
                        Оказывает любые виды услуг по программам.<br>
                        Также мы предоставляем полный комплекс услуг по обновлению<br>
                        и поддержке программных продуктов на платформе 1С.
                    </div>
                </div>
                <div class="col-12 col-md-5 text-center">
                    <div class="achievement mx-auto">
                        <div class="h1 achievement__number">250</div>
                        <div class="achievement__text">Лучших сотрудников</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="d-flex flex-wrap mb-50 justify-content-center"
             onclick="goToSpecialistCard('specialist-card-1');">
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
            <div class="team-participant" style="background-image: url('./images/team-man.png')"
                 data-toggle="tab" data-target="#specialist-1" data-role="tab">
                <div class="team-participant__about">
                    <div class="h4">Петров Александр</div>
                    <div>Руководитель отдела</div>
                </div>
            </div>
        </div>
        <a name="specialist-card-1"></a>
        <div class="tab-content">
            <div class="tab-pane fade" id="specialist-1" role="tabpanel">
                <div class="card shadow-lg specialist-card mb-40">
                    <div class="card-body">
                        <div class="row no-gutters">
                            <div class="col-12 col-lg-3 d-flex align-items-end mb-md-n20">
                                <img src="./images/team-participant.png">
                            </div>
                            <div class="col-12 col-lg-5 d-flex flex-column justify-content-center">
                                <div class="h3 mb-30">Руководитель отдела</div>
                                <div class="line-75 mb-30"></div>
                                <div class="h2 mb-30">Петров Александр</div>
                                <div class="mb-20">Обеспечение порядка</div>
                                <div class="small text-gray font-weight-light">
                                    <div class="mb-20">
                                        <span class="font-weight-bold">Опыт работы:</span> 10 лет
                                    </div>
                                    <div class="mb-20">
                                        <span class="font-weight-bold">Навыки:</span>
                                        Проходят серьезное обучение<br/>
                                        Высококвалифицированны<br/>
                                        Особый подход
                                    </div>
                                </div>
                            </div>
                            <div class="col-12 col-lg-4 d-flex align-items-end">
                                <div class="glide" data-glide-perview="2">
                                    <div class="glide__track" data-glide-el="track">
                                        <ul class="glide__slides">
                                            <li class="glide__slide">
                                                <a data-fancybox href="./images/certificate.png">
                                                    <img src="./images/certificate.png"></a>
                                            </li>
                                            <li class="glide__slide">
                                                <a data-fancybox href="./images/certificate.png">
                                                    <img src="./images/certificate.png"></a>
                                            </li>
                                        </ul>
                                        <div data-glide-el="controls" class="text-center">
                                            <button data-glide-dir="<"
                                                    class="btn btn-sm btn-secondary-faded btn-circle mr-2">
                                                <i class="icon-arrow-left"></i>
                                            </button>
                                            <button data-glide-dir=">"
                                                    class="btn btn-sm btn-secondary-faded btn-circle ">
                                                <i class="icon-arrow-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <button class="btn btn-secondary-faded">Получить консультацию</button>
            </div>
        </div>
        <div class="text-center">
            <a href="#" class="btn btn-secondary-faded px-40">Смотреть подробнее</a>
        </div>
    </div>
</section>