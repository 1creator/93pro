<section class="py-60 text-primary text-right" style="background-image: url('./images/bg-state-companies.jpg')">
    <div class="container">
        <div class="row flex-column-reverse flex-md-row">
            <div class="col-12 col-md-4 text-center text-md-left mb-40 mb-md-0">
                <img src="./images/star.svg">
                <img src="./images/star.svg">
                <img src="./images/star.svg">
                <img src="./images/star.svg">
                <img src="./images/star.svg">
            </div>
            <div class="col-12 col-md-8">
                <div class="h1 mb-30">
                    Работаем с
                    государственными
                    компаниями
                </div>
                <div class="font-weight-light mb-60" style="letter-spacing: 1.3px;">
                    Компания учавствует в тендерах на поставку ПО, оказание услуг<br class="d-none d-lg-inline"/>
                    и выполнение работ по объявленным в документах условиям
                </div>
                <div class="d-flex mb-md-30 flex-column flex-md-row justify-content-end">
                    <div class="h3 mb-30 mb-md-0">Закупка в соответствии
                        с номерами 223-ФЗ</div>
                    <div class="h3 mb-30 mb-md-0 ml-md-50">Закупка в соответствии
                        с номерами 44-ФЗ</div>
                </div>
                <div class="d-flex mb-md-60 flex-column flex-md-row justify-content-end">
                    <div class="h3 mb-30 mb-md-0">Закупка в соответствии
                        с номерами 223-ФЗ</div>
                    <div class="h3 mb-30 mb-md-0 ml-md-50">Закупка в соответствии
                        с номерами 44-ФЗ</div>
                </div>
            </div>
        </div>
        <div class="text-center text-md-right">
            <a href="#" class="btn btn-sm btn-secondary-faded">Подробнее</a>
        </div>
    </div>
</section>