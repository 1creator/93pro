<div class="glide offers-slider" data-glide-perview="1">
    <div class="glide__track" data-glide-el="track">
        <ul class="glide__slides">
            <li class="glide__slide">
                <div class="offer bg-primary text-white">
                    <div class="container position-relative">
                        <div class="row align-items-center">
                            <div class="col-12 col-md-8 text-center text-md-left">
                                <div class="offer__content">
                                    <div class="h1 offer__title">
                                        <span class="text-secondary-faded">20% скидка</span><br/>
                                        на обслуживание в мае!
                                    </div>
                                    <div class="offer__text">
                                        Оказывает любые виды услуг по программам.<br/>
                                        Также мы предоставляем полный комплекс услуг по обновлению<br/>
                                        и поддержке программных продуктов на платформе 1С.
                                    </div>
                                    <a href="/offers" class="btn btn-secondary-faded offer__btn">Акции</a>
                                </div>
                            </div>
                            <div class="offer__bg-image mr-n130">
                                <img src="/images/man.png">
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
        <div class="mx-auto offers-slider__controls d-none d-md-block">
            <div class="col text-center" data-glide-el="controls">
                <button data-glide-dir="<" class="btn btn-sm mr-2 btn-outline-secondary-faded btn-circle">
                    <i class="icon-arrow-left"></i>
                </button>
                <button data-glide-dir=">" class="btn btn-sm btn-outline-secondary-faded btn-circle">
                    <i class="icon-arrow-right"></i>
                </button>
            </div>
        </div>
    </div>
</div>