<section class="py-200 text-primary">
    <div class="container bg-93-top-right">
        <div class="d-flex align-items-center mb-90 flex-column flex-md-row">
            <h3 class="h1 mb-30 mb-md-0" style="letter-spacing: 0.2px;">Кто мы и что предлагаем</h3>
            <a href="./about" class="btn btn-secondary-faded btn-sm ml-md-70">
                О компании
            </a>
        </div>
        <div class="row">
            <div class="col-12 col-md-5 mb-60">
                <img class="icon-80 mb-30" src="./images/who-are-we-icon.png">
                <div class="h3 mb-3 letter-spacing-0">Официальный франчайзи 1С</div>
                <div class="font-weight-light">
                    Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы предоставляем
                    полный комплекс услуг\
                </div>
            </div>
            <div class="col-12 col-md-5 offset-md-1 mb-60">
                <img class="icon-80 mb-30" src="./images/who-are-we-icon.png">
                <div class="h3 mb-3 letter-spacing-0">Работаем с гос. компаниями</div>
                <div class="font-weight-light">
                    Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы предоставляем
                    полный комплекс услуг\
                </div>
            </div>
            <div class="col-12 col-md-5 mb-60 mb-md-0">
                <img class="icon-80 mb-30" src="./images/who-are-we-icon.png">
                <div class="h3 mb-3 letter-spacing-0">Только квалифицированные
                    и сертифицированные сотрудники</div>
                <div class="font-weight-light">
                    Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы предоставляем
                    полный комплекс услуг по обновлению и поддержке программных продуктов на платформе 1С:Предприятие
                    7.7.
                </div>
            </div>
            <div class="col-12 col-md-5 offset-md-1 mb-60 mb-md-0">
                <img class="icon-80 mb-30" src="./images/who-are-we-icon.png">
                <div class="h3 mb-3 letter-spacing-0">Работаем с бюджетными
                    организациями</div>
                <div class="font-weight-light">
                    Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы предоставляем
                    полный комплекс услуг по обновлению и поддержке программных продуктов на платформе 1С:Предприятие
                    7.7.
                </div>
            </div>
        </div>
    </div>
</section>