<section class="py-60 text-white" style="background-image: url('./images/bg-international-work.jpg')">
    <div class="container">
        <div class="row flex-column-reverse flex-md-row">
            <div class="col-12 col-md-8">
                <h2 class="h1 mb-30">
                    Работаем с
                    иностранными
                    компаниями
                </h2>
                <div class="font-weight-thin mb-60" style="letter-spacing: 1.3px">
                    Более 10 лет мы занимаемся обслуживанием иностранных компаний в
                    <br class="d-none d-lg-inline"/>России и оказываем широкий спектр
                    услуг для иностранных <br class="d-none d-lg-inline"/>представительств/филиалов
                    и дочерних компаний: комплексные <br class="d-none d-lg-inline"/>бухгалтерские
                    услуги, расчет зарплат и услуги кадрового делопроизводства,
                    <br class="d-none d-lg-inline"/>а также юридические и миграционные услуги.
                </div>
            </div>
            <div class="col-12 col-md-4 text-center text-md-right mb-60 mb-md-0">
                <img src="./images/star.svg">
                <img src="./images/star.svg">
                <img src="./images/star.svg">
                <img src="./images/star.svg">
                <img src="./images/star.svg">
            </div>
        </div>
        <div class="d-flex mb-60 flex-column flex-md-row">
            <div class="h3 mb-30 mb-md-0">Понимание<br/>
                менталитета</div>
            <div class="h3 mb-30 mb-md-0 ml-md-50">Подразделения<br/>
                в России </div>
            <div class="h3 mb-30 mb-md-0 ml-md-50">Позитивный<br/>
                опыт</div>
        </div>
        <div class="text-center text-md-left">
            <a href="#" class="btn btn-sm btn-secondary-faded">Подробнее</a>
        </div>
    </div>
</section>