<section class="py-0">
    <div class="bg-primary py-50">
        <div class="container">
            <div class="row">
                <div class="col-12 col-md-4">
                    <div class="achievement">
                        <div class="h1 achievement__number">250</div>
                        <div class="achievement__text">Постоянных клиентов</div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="achievement">
                        <div class="h1 achievement__number">15</div>
                        <div class="achievement__text">Лет опыта</div>
                    </div>
                </div>
                <div class="col-12 col-md-4">
                    <div class="achievement">
                        <div class="h1 achievement__number">250</div>
                        <div class="achievement__text">Лучших сотрудников</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>