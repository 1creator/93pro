<section class="py-200">
    <div class="container px-md-60">
        <div class="glide" data-glide-perview="1" data-glide-gap="0">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                    <li class="glide__slide">
                        <div class="text-review">
                            <img src="./images/avatar.png">
                            <div class="h3 mb-30">Петров Александр</div>
                            <div class="mb-20">Спасибо большое за проделанную работу!
                                <br class="d-none d-md-inline"/>Прекрасные специалисты и
                                <br class="d-none d-md-inline"/>качественные услуги
                            </div>
                            <a href="#" class="btn btn-link">ЧИТАТЬ ДАЛЬШЕ</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div data-glide-el="controls">
                <button data-glide-dir="<"
                        class="btn btn-sm btn-outline-secondary-faded btn-circle glide__arrow glide__arrow--left">
                    <i class="icon-arrow-left"></i>
                </button>
                <button data-glide-dir=">"
                        class="btn btn-sm btn-outline-secondary-faded btn-circle glide__arrow glide__arrow--right">
                    <i class="icon-arrow-right"></i>
                </button>
            </div>
        </div>
    </div>
</section>