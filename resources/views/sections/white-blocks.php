<section class="py-0 section-you-interested">
    <div class="container">
        <div class="row no-gutters shadow-lg">
            <div class="col-12 col-md-4">
                <div class="card card-service bg-white text-left">
                    <div class="card-body align-items-start">
                        <div class="h2 card-title mb-3">Поддержка</div>
                        <div class="mb-3 font-weight-light">
                            Отслеживание трат
                            Отслеживание задолженности
                            Следование плану
                            Сравнение показателей
                            Управление бизнесом
                        </div>
                        <button class="btn btn-dark position-static">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="card card-service bg-white text-left">
                    <div class="card-body align-items-start">
                        <div class="h2 card-title mb-3">Поддержка</div>
                        <div class="mb-3 font-weight-light">
                            Отслеживание трат
                            Отслеживание задолженности
                            Следование плану
                            Сравнение показателей
                            Управление бизнесом
                        </div>
                        <button class="btn btn-dark position-static">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4">
                <div class="card card-service bg-white text-left">
                    <div class="card-body align-items-start">
                        <div class="h2 card-title mb-3">Поддержка</div>
                        <div class="mb-3 font-weight-light">
                            Отслеживание трат
                            Отслеживание задолженности
                            Следование плану
                            Сравнение показателей
                            Управление бизнесом
                        </div>
                        <button class="btn btn-dark position-static">Подробнее</button>
                    </div>
                </div>
            </div>
        </div>
</section>