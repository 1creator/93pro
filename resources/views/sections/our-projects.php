<section class="py-200">
    <div class="container">
        <div class="d-flex align-items-center text-primary mb-70 flex-column flex-md-row">
            <h2 class="h1 mb-30 mb-md-0">Наши проекты</h2>
            <a href="#" class="btn btn-secondary-faded ml-md-60 px-30">Смотреть все проекты</a>
        </div>
    </div>
    <div class="glide" data-glide-perview="1">
        <div class="glide__track" data-glide-el="track">
            <ul class="glide__slides">
                <li class="glide__slide">
                    <div class="container">
                        <div class="project-preview" style="background-image: url('./images/bg-office-work.jpg')">
                            <div class="project-preview__header">
                                <div class="project-preview-header__date">
                                    <div class="h1">21</div>
                                    <div>Января 2019</div>
                                </div>
                                <div class="project-preview-header__title">
                                    <a class="h2" href="">Внедрение 1С</a>
                                    <div>ОАО "Воентелеком"</div>
                                    <a href="#" class="btn btn-link pl-0">ЧИТАТЬ ДАЛЬШЕ</a>
                                </div>
                                <div class="project-preview-header__stars ml-auto">
                                    <img src="./images/star.svg">
                                    <img src="./images/star.svg">
                                    <img src="./images/star.svg">
                                    <img src="./images/star.svg">
                                    <img src="./images/star.svg">
                                </div>
                            </div>
                            <div class="py-130 project-preview-body">
                                <div class="h3 project-preview-body__header">
                                    Нужны ли услуги программиста 1С? Разумеется, нет! Кому
                                    нужны отлаженные бизнес-процессы</div>
                                <div class="project-preview-body__text">
                                    Уверенность в стабильной работе программы и
                                    индивидуальные&nbsp;решения для бизнеса, которые сэкономят&nbsp;деньги
                                    и рабочее время
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
        <div class="mx-auto d-none d-md-block mt-40">
            <div class="col text-center" data-glide-el="controls">
                <button data-glide-dir="<" class="btn btn-sm btn-outline-secondary-faded btn-circle">
                    <i class="icon-arrow-left"></i>
                </button>
                <span class="slider-indices mx-2 text-primary font-montserrat font-weight-medium">1/5</span>
                <button data-glide-dir=">" class="btn btn-sm btn-outline-secondary-faded btn-circle">
                    <i class="icon-arrow-right"></i>
                </button>
            </div>
        </div>
    </div>
</section>