<section class="py-200">
    <div class="container">
        <h2 class="h1 mb-20">Отраслевые решения</h2>
        <div class="row">
            <div class="col-12 col-md-10">
                <div class="mb-80 font-weight-light">
                    Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы
                    предоставляем полный
                    комплекс услуг по обновлению и поддержке программных продуктов на платформе 1С:Предприятие 7.7.
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-secondary-faded">
                    <div class="card-body">
                        <div class="h2 card-title">
                            Сельское<br class="d-none d-lg-inline"/>
                            и лесное<br class="d-none d-lg-inline"/>
                            хозяйство
                        </div>
                        <button class="btn btn-dark btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-primary">
                    <div class="card-body">
                        <div class="h2 card-title">
                            Образование,<br class="d-none d-lg-inline"/>
                            культура
                        </div>
                        <button class="btn btn-light btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-secondary-faded">
                    <div class="card-body">
                        <div class="h2 card-title">
                            Производство,<br class="d-none d-lg-inline"/>
                            ТЭК
                        </div>
                        <button class="btn btn-dark btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-primary">
                    <div class="card-body">
                        <div class="h2 card-title">Аренда 1С</div>
                        <button class="btn btn-light btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-secondary-faded">
                    <div class="card-body">
                        <div class="h2 card-title">СБИСС++</div>
                        <button class="btn btn-dark btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-primary">
                    <div class="card-body">
                        <div class="h2 card-title">
                            Сопровождение<br class="d-none d-lg-inline"/>
                            и сервисы 1С (ИТС)
                        </div>
                        <button class="btn btn-light btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-secondary-faded">
                    <div class="card-body">
                        <div class="h2 card-title">
                            Разработка<br class="d-none d-lg-inline"/>
                            и другие услуги
                        </div>
                        <button class="btn btn-dark btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-primary">
                    <div class="card-body">
                        <div class="h2 card-title">Обучение</div>
                        <button class="btn btn-light btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-secondary-faded">
                    <div class="card-body">
                        <div class="h2 card-title">
                            Ускорение<br class="d-none d-lg-inline"/>
                            работы 1С
                        </div>
                        <button class="btn btn-dark btn-sm">Подробнее</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>