<section class="py-200 section-you-need-us">
    <div class="container">
        <h2 class="mb-40">Мы нужны вам чтобы:</h2>
        <div class="row text-additional font-weight-light">
            <div class="col-12 col-md-4 mb-70">
                <div class="h1 font-weight-medium text-secondary-faded">1.</div>
                <div class="h3">
                    Мониторинг и <br class="d-none d-lg-inline"/>анализ показателей
                    <br class="d-none d-lg-inline"/>деятельности
                    <br class="d-none d-lg-inline"/>предприятия с
                    расшифровками <br class="d-none d-lg-inline"/>исходных данных
                </div>
            </div>
            <div class="col-12 col-md-4 mb-70">
                <div class="h1 font-weight-medium text-secondary-faded">2.</div>
                <div class="h3">
                    Управление производством
                    <br class="d-none d-lg-inline"/>используя инструменты управления для
                    <br class="d-none d-lg-inline"/>различных видов и
                    <br class="d-none d-lg-inline"/>типов производства
                </div>
            </div>
            <div class="col-12 col-md-4 mb-70">
                <div class="h1 font-weight-medium text-secondary-faded">3.</div>
                <div class="h3">
                    Управление затратами и расчет себестоимости. Предоставление данных о структуре себестоимости выпуска
                    до первичных затрат
                </div>
            </div>
            <div class="col-12 col-md-4 mb-70">
                <div class="h1 font-weight-medium text-secondary-faded">4.</div>
                <div class="h3">
                    Организация
                    <br class="d-none d-lg-inline"/>ремонтов плановых
                    <br class="d-none d-lg-inline"/>и внеплановых проведение
                    <br class="d-none d-lg-inline"/>ремонтных
                    <br class="d-none d-lg-inline"/>мероприятий
                </div>
            </div>
            <div class="col-12 col-md-4 mb-70">
                <div class="h1 font-weight-medium text-secondary-faded">5.</div>
                <div class="h3">
                    Управление взаимоотношениями с клиентами.
                    <br class="d-none d-lg-inline"/>Бизнес-процессы организации взаимодействия с клиентами,
                    карты лояльности
                </div>
            </div>
            <div class="col-12 col-md-4 mb-70">
                <div class="h1 font-weight-medium text-secondary-faded">6.</div>
                <div class="h3">
                    Управление закупками. Бизнес-процессы
                    <br class="d-none d-lg-inline"/>сложных закупок,
                    <br class="d-none d-lg-inline"/>контроль условий
                    <br class="d-none d-lg-inline"/>поставок
                </div>
            </div>
        </div>
    </div>
</section>