<section class="py-0">
    <div class="container">
        <div class="d-flex text-primary align-items-center mb-70 flex-column flex-md-row">
            <h3 class="h1 mb-30 mb-md-0">Видеообзор</h3>
            <a href="#" class="btn btn-secondary-faded ml-md-auto">Перейти к отзывам</a>
        </div>
        <div class="glide" data-glide-perview="1">
            <div class="glide__track" data-glide-el="track">
                <ul class="glide__slides">
                    <li class="glide__slide">
                        <div class="row">
                            <div class="col-12 col-md-6 mb-30">
                                <div class="video-review-mini">
                                    <iframe src="https://www.youtube.com/embed/HfnINdtw_8E"
                                            frameborder="0"
                                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                                            allowfullscreen></iframe>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-30">
                                <div class="video-review-mini">
                                    <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-30">
                                <div class="video-review-mini">
                                    <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-30">
                                <div class="video-review-mini">
                                    <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-30">
                                <div class="video-review-mini">
                                    <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-30">
                                <div class="video-review-mini">
                                    <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-30 mb-md-0">
                                <div class="video-review-mini">
                                    <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mb-30 mb-md-0">
                                <div class="video-review-mini">
                                    <button class="btn btn-light btn-circle"><i class="icon-triangle-right"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="mx-auto d-none d-md-block mt-40">
                <div class="col text-center" data-glide-el="controls">
                    <button data-glide-dir="<" class="btn btn-sm btn-outline-secondary-faded btn-circle">
                        <i class="icon-arrow-left"></i>
                    </button>
                    <span class="slider-indices mx-2 text-primary font-montserrat font-weight-medium">1/5</span>
                    <button data-glide-dir=">" class="btn btn-sm btn-outline-secondary-faded btn-circle">
                        <i class="icon-arrow-right"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</section>