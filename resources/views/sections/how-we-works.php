<section>
    <div class="container">
        <h2 class="h1 mb-80">Как мы работаем</h2>
        <div class="row no-gutters">
            <div class="col-12 col-md-3 algorithm-item">
                <h3 class="h2 algorithm-item__header">1 этап</h3>
                <div class="algorithm-item__text">
                    <div class="h4">Проектирование</div>
                    Составление технического задания<br/><br/>
                    Обучение ответственных пользователей<br/><br/>
                    Определяются конкретные требования к&nbsp;результатам внедрения
                </div>
            </div>
            <div class="col-12 col-md-3 algorithm-item">
                <h3 class="h2 algorithm-item__header">2 этап</h3>
                <div class="algorithm-item__text">
                    <div class="h4">Проектирование</div>
                    Составление технического задания<br/><br/>
                    Обучение ответственных пользователей<br/><br/>
                    Определяются конкретные требования к&nbsp;результатам внедрения
                </div>
            </div>
            <div class="col-12 col-md-3 algorithm-item">
                <h3 class="h2 algorithm-item__header">3 этап</h3>
                <div class="algorithm-item__text">
                    <div class="h4">Проектирование</div>
                    Составление технического задания<br/><br/>
                    Обучение ответственных пользователей<br/><br/>
                    Определяются конкретные требования к&nbsp;результатам внедрения
                </div>
            </div>
            <div class="col-12 col-md-3 algorithm-item">
                <h3 class="h2 algorithm-item__header">4 этап</h3>
                <div class="algorithm-item__text">
                    <div class="h4">Проектирование</div>
                    Составление технического задания<br/><br/>
                    Обучение ответственных пользователей<br/><br/>
                    Определяются конкретные требования к&nbsp;результатам внедрения
                </div>
            </div>
        </div>
    </div>
</section>