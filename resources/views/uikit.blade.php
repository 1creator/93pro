<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>UI Kit</title>
    <link rel="stylesheet" type="text/css" href="./css/app.css">
    <link rel="stylesheet" type="text/css" href="./css/uikit.css">
    <script src="./js/app.js"></script>
</head>
<body class="uikit">
<section class="uikit-section">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h4 class="uikit-section__header mb-4">01. Модульная сетка</h4>
                <div class="row mb-3" style="height: 200px; opacity: 0.2">
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                    <div class="col">
                        <div class="bg-secondary h-100"></div>
                    </div>
                </div>
                <div class="small text-center">
                    Grid 12 - Width 1140px / Offset 36px Gutter width - 30px Column width - 65 px
                </div>
            </div>
        </div>
    </div>
</section>
<section class="uikit-section">
    <div class="container">
        <div class="row">
            <div class="col-12 colors">
                <h4 class="uikit-section__header mb-4">02. Цвета</h4>
                <div class="row">
                    <div class="col">
                        <div class="colors-item">
                            <div class="colors-item__circle bg-primary"></div>
                            <div class="colors-item__name">Main</div>
                            <div class="colors-item__code text-additional">232531</div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="colors-item">
                            <div class="colors-item__circle bg-secondary"></div>
                            <div class="colors-item__name">Secondary</div>
                            <div class="colors-item__code text-additional">F5A623</div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="colors-item">
                            <div class="colors-item__circle bg-additional"></div>
                            <div class="colors-item__name">+</div>
                            <div class="colors-item__code text-additional">9B9B9B</div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="colors-item">
                            <div class="colors-item__circle bg-black"></div>
                            <div class="colors-item__name">Текст</div>
                            <div class="colors-item__code text-additional">#000000</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="uikit-section typography">
    <div class="container">
        <h4 class="uikit-section__header mb-4">03. Типографика</h4>
        <div class="typography__items">
            <h1>Заголовок/Montserrat/48px/Extrabold</h1>
            <h2>Обычный заголовок/Montserrat/32px/Bold</h2>
            <h3>Средний заголовок/Montserrat/24px/Вold</h3>
            <h4>Мини заголовок/Montserrat/20px/Bold</h4>
            <div>Основной текст/Helvetica Neue/18px/Regular</div>
            <div class="small">Маленький текстHelvetica Neue/14px/Regular</div>
            <div class="btn">Текст кнопок/Roboto/16px/Regular</div>
            <div>
                <div class="btn btn-link text-black font-weight-bold">Текстовая кнопка/Helvetica neue/14px/Condensed
                    bold
                </div>
            </div>
        </div>
    </div>
</section>
<section class="uikit-section typography-sample">
    <div class="container">
        <h4 class="uikit-section__header mb-4">04. Типографика</h4>
        <h3 class="text-primary mb-3">Официальный франчайзи 1С</h3>
        <div class="row mb-5">
            <div class="col-12 col-sm-5">
                Оказывает любые виды услуг по программам семейства 1С:Предприятие 8.2, 8.3. Также мы предоставляем
                полный комплекс услуг по обновлению и поддержке программных продуктов на платформе 1С:Предприятие 7.7.
            </div>
        </div>
        <table class="table table-borderless mb-5">
            <tbody>
            <tr>
                <td>
                    <div class="h3 text-primary">ИТС ПРОФ Комфортный</div>
                </td>
                <td>
                    <div class="text-nowrap">1 месяц</div>
                </td>
                <td>
                    <div class="font-weight-bold">от 1 000р.</div>
                </td>
                <td>
                    <button class="btn btn-secondary-faded">Выбрать</button>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="h3 text-primary">ИТС ПРОФ Комфортный</div>
                </td>
                <td>
                    <div class="text-nowrap">3 месяца</div>
                </td>
                <td>
                    <div class="font-weight-bold">от 1 000р.</div>
                </td>
                <td>
                    <button class="btn btn-secondary-faded">Выбрать</button>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="h3 text-primary">ИТС ПРОФ Комфортный</div>
                </td>
                <td>
                    <div class="text-nowrap">5 месяцев</div>
                </td>
                <td>
                    <div class="font-weight-bold">от 1 000р.</div>
                </td>
                <td>
                    <button class="btn btn-secondary-faded">Выбрать</button>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
</section>
<section class="uikit-section">
    <div class="container">
        <div class="row">
            <div class="col">
                <h3 class="text-primary mb-3">Для чего используют «1С:ERP»?</h3>
                <ul>
                    <li>Своевременное обновление конфигураций</li>
                    <li>Своевременное обновление конфигураций, дает</li>
                    <li>Своевременное обновление конфигураций, дает на получение обновление конфигураций</li>
                    <li>Своевременное обновление конфигураций, дает</li>
                    <li>Своевременное обновление конфигураций</li>
                </ul>
            </div>
        </div>
    </div>
</section>
<header>
    <nav class="nav navbar">
        <div class="container">
            <div class="nav-item">
                <a href="./programs.html">Программы</a>
            </div>
            <div class="nav-item">
                <a href="./services.html">Услуги</a>
            </div>
            <div class="nav-item">
                <a href="./industries.html">Отрасли</a>
            </div>
            <div class="nav-item">
                <a href="./offers.html">Акции</a>
            </div>
            <div class="nav-item">
                <a href="./projects.html">Проекты</a>
            </div>
            <div class="nav-item">
                <a href="./price.html">Прайс</a>
            </div>
            <div class="nav-item">
                <a href="./about.html">О нас</a>
            </div>
            <div class="nav-item">
                <a href="./blog.html">Блог</a>
            </div>
            <div class="nav-item ml-20">
                <div class="input-icon">
                    <input class="form-control" type="search">
                    <i class="input-icon__icon icon-search"></i>
                </div>
            </div>
            <div class="nav-item">
                <button class="btn btn-secondary-faded">Заказать звонок</button>
            </div>
        </div>
    </nav>
</header>
<section>
    <div class="container">
        <div class="row">
            <div class="col py-20 bg-primary text-right">
                <div class="dropdown">
                    <a class="btn btn-outline-light" href="#" role="button" data-toggle="dropdown" aria-haspopup="true"
                       aria-expanded="false">
                        Санатории
                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuLink">
                        <a class="dropdown-item" href="#">Поликлиники, Стационары, Скорая</a>
                        <a class="dropdown-item" href="#">Санатории</a>
                        <a class="dropdown-item" href="#">Управления здравоохранением</a>
                        <a class="dropdown-item" href="#">Фармацевтика</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="uikit-section interface">
    <div class="container">
        <h4 class="uikit-section__header mb-5">05. Элементы интерфейса</h4>
        <div class="row mb-5">
            <div class="col-12 col-lg-7 mb-3">
                <div class="row">
                    <div class="col-12 col-sm-4 mb-3">
                        <button class="btn btn-secondary-faded w-100">Акции</button>
                    </div>
                    <div class="col-12 col-sm-4 mb-3">
                        <button class="btn btn-secondary-faded w-100">Акции</button>
                    </div>
                    <div class="col-12 col-sm-4 mb-3">
                        <button class="btn btn-secondary-faded w-100">Акции</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-4 mb-3">
                        <button class="btn btn-dark w-100">Заказать звонок</button>
                    </div>
                    <div class="col-12 col-sm-4 mb-3">
                        <button class="btn btn-dark w-100">Заказать звонок</button>
                    </div>
                    <div class="col-12 col-sm-4 mb-3">
                        <button class="btn btn-dark w-100">Заказать звонок</button>
                    </div>
                </div>
                <div class="row bg-primary py-20 mb-3">
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-secondary w-100 mb-3 mb-md-0">Акции</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-secondary w-100 mb-3 mb-md-0">Акции</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-secondary w-100">Акции</button>
                    </div>
                </div>
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-link w-100">ЧИТАТЬ ДАЛЬШЕ</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-link w-100">ЧИТАТЬ ДАЛЬШЕ</button>
                    </div>
                    <div class="col-12 col-sm-4">
                        <button class="btn btn-link w-100">ЧИТАТЬ ДАЛЬШЕ</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-12 col-lg-6 mb-3">
                <div class="form-row mb-4">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Имя">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Номер телефона">
                    </div>
                </div>
                <div class="form-row form-validation">
                    <div class="col">
                        <div class="input-validation">
                            <input type="text" class="form-control" placeholder="Имя" required value="Иван">
                            <i class="input-validation__icon"></i>
                        </div>
                    </div>
                    <div class="col">
                        <div class="input-validation">
                            <input type="text" class="form-control" placeholder="Номер телефона" required>
                            <i class="input-validation__icon"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-6">
                <div class="row">
                    <div class="col-6">
                        <div class="row">
                            <div class="col-12">
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" id="radio1" name="radio" class="custom-control-input">
                                    <label class="custom-control-label" for="radio1"></label>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="custom-control custom-radio mb-3">
                                    <input type="radio" id="radio2" name="radio" checked class="custom-control-input">
                                    <label class="custom-control-label" for="radio2"></label>
                                </div>
                            </div>
                            <div class="col-6 mb-3"><i class="text-secondary icon-caret-up"></i></div>
                            <div class="col-6 mb-3"><i class="text-secondary icon-caret-right"></i></div>
                            <div class="col-6 mb-3"><i class="text-secondary icon-caret-down"></i></div>
                            <div class="col-6 mb-3"><i class="text-secondary icon-caret-left"></i></div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="row">
                            <div class="col-6 mb-3">
                                <div class="bg-primary p-3 text-center">
                                    <button class="btn btn-primary btn-circle"><i class="icon-close"></i></button>
                                </div>
                            </div>
                            <div class="col-6 mb-3">
                                <div class="p-3 text-center">
                                    <button class="btn btn-light btn-circle"><i class="icon-close"></i></button>
                                </div>
                            </div>
                            <div class="col-4 mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" checked class="custom-control-input" id="check1">
                                    <label class="custom-control-label" for="check1"></label>
                                </div>
                            </div>
                            <div class="col-4 mb-3">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" class="custom-control-input" id="check2">
                                    <label class="custom-control-label" for="check2"></label>
                                </div>
                            </div>
                            <div class="col-4 mb-3">
                                <i class="text-secondary icon-search"></i>
                            </div>
                            <div class="col-6 mb-3">
                                <i class="text-secondary icon-sm icon-caret-down"></i>
                                <i class="text-secondary icon-sm icon-caret-up"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-12 col-md-4">
                <div class="row mb-5">
                    <div class="col-4">
                        <button class="btn btn-sm btn-secondary-faded btn-circle mr-1">
                            <i class="icon-arrow-left"></i>
                        </button>
                        <button class="btn btn-sm btn-secondary-faded btn-circle">
                            <i class="icon-arrow-right"></i>
                        </button>
                    </div>
                    <div class="col-4">
                        <button class="btn btn-sm btn-secondary-faded btn-circle mr-1">
                            <i class="icon-arrow-left"></i>
                        </button>
                        <button class="btn btn-sm btn-secondary-faded btn-circle">
                            <i class="icon-arrow-right"></i>
                        </button>
                    </div>
                    <div class="col-4">
                        <button class="btn btn-sm btn-secondary-faded btn-circle mr-1">
                            <i class="icon-arrow-left"></i>
                        </button>
                        <button class="btn btn-sm btn-secondary-faded btn-circle">
                            <i class="icon-arrow-right"></i>
                        </button>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <input class="w-100" type="range" min="0" max="100">
                    </div>
                </div>
            </div>
            <div class="col-12 col-md-4 offset-md-4">
                <div class="progress mb-5">
                    <div class="progress-bar" role="progressbar"
                         style="width: 40%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100">40%
                    </div>
                </div>
                <div class="progress mb-5">
                    <div class="progress-bar" role="progressbar"
                         style="width: 90%;" aria-valuenow="90" aria-valuemin="0" aria-valuemax="100">90%
                    </div>
                </div>
                <div class="progress mb-5">
                    <div class="progress-bar" role="progressbar"
                         style="width: 100%;" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100">100%
                    </div>
                </div>
                <div class="progress">
                    <div class="progress-bar" role="progressbar"
                         style="width: 60%;" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100">60%
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="uikit-section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-4">
                <div class="card card-client">
                    <div class="card-client__about">
                        <h1>1.</h1>
                        <h3>Директора и руководители ИТ&nbsp;отделов</h3>
                        <div>Отслеживание трат
                            Отслеживание задолженности
                            Следование плану
                            Сравнение показателей
                            Управление бизнесом
                        </div>
                    </div>
                    <div class="card card-client__services bg-secondary-faded">
                        <h4>Вам может подойти</h4>
                        <a href="#">Установка 1С</a>
                        <a href="#">Поддержка 1С</a>
                        <a href="#">Постановка управленческого и&nbsp;финансового учета</a>
                        <a href="#">Программирование 1С</a>
                        <a href="#">Оптимизация работы систем 1С</a>
                        <a href="#">Продажа программных продуктов 1С</a>
                        <a href="#">Консалтинговые услуги</a>
                        <a href="#">Курсы 1С:Бухгалтерия 8 и 1С:</a>
                        <a href="#">Предприятие 8</a>
                        <a href="#">Обучение 1С</a>
                        <a href="#">1С: ИТС</a>
                        <div>
                            <!--ask hover button by hover card or no-->
                            <button class="btn btn-dark">Подробнее</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-client">
                    <div class="card-client__about">
                        <h1>2.</h1>
                        <h3>Сисадмины - лица занимающиеся ИТ поддержкой</h3>
                        <div>Отслеживание трат
                            Отслеживание задолженности
                            Следование плану
                            Сравнение показателей
                            Управление бизнесом
                        </div>
                    </div>
                    <div class="card card-client__services bg-primary">
                        <h4>Вам может подойти</h4>
                        <a href="#">Установка 1С</a>
                        <a href="#">Поддержка 1С</a>
                        <a href="#">Постановка управленческого и&nbsp;финансового учета</a>
                        <a href="#">Программирование 1С</a>
                        <a href="#">Оптимизация работы систем 1С</a>
                        <a href="#">Продажа программных продуктов 1С</a>
                        <a href="#">Консалтинговые услуги</a>
                        <a href="#">Курсы 1С:Бухгалтерия 8 и 1С:</a>
                        <a href="#">Предприятие 8</a>
                        <a href="#">Обучение 1С</a>
                        <a href="#">1С: ИТС</a>
                        <div>
                            <button class="btn btn-light">Подробнее</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-client">
                    <div class="card-client__about">
                        <h1>3.</h1>
                        <h3>Компании-подрядчики, оказывающие близкие услуги</h3>
                        <div>Отслеживание трат
                            Отслеживание задолженности
                            Следование плану
                            Сравнение показателей
                            Управление бизнесом
                        </div>
                    </div>
                    <div class="card card-client__services bg-secondary-faded">
                        <h4>Вам может подойти</h4>
                        <a href="#">Установка 1С</a>
                        <a href="#">Поддержка 1С</a>
                        <a href="#">Постановка управленческого и&nbsp;финансового учета</a>
                        <a href="#">Программирование 1С</a>
                        <a href="#">Оптимизация работы систем 1С</a>
                        <a href="#">Продажа программных продуктов 1С</a>
                        <a href="#">Консалтинговые услуги</a>
                        <a href="#">Курсы 1С:Бухгалтерия 8 и 1С:</a>
                        <a href="#">Предприятие 8</a>
                        <a href="#">Обучение 1С</a>
                        <a href="#">1С: ИТС</a>
                        <div>
                            <button class="btn btn-dark">Подробнее</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="uikit-section">
    <div class="container">
        <div class="row mb-5 no-gutters">
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-secondary-faded">
                    <div class="card-body">
                        <h2 class="card-title">Внедрение 1С</h2>
                        <button class="btn btn-dark">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-primary">
                    <div class="card-body">
                        <h2 class="card-title">Интеграция с 1С</h2>
                        <!--button hover effect???-->
                        <button class="btn btn-light">Подробнее</button>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-4">
                <div class="card card-service bg-secondary-faded">
                    <div class="card-body">
                        <h2 class="card-title">Обслуживание 1С</h2>
                        <button class="btn btn-dark">Подробнее</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row justify-content-between">
            <div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-1">
                    Модал 1
                </button>
                <div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title text-primary">Мы <span
                                            class="text-secondary-faded">с радостью</span> свяжемся с вами!</h2>
                                <div>Если у вас остались вопросы, вы можете оставить
                                    номер <br class="d-none d-lg-inline"/>телефона и мы с вами
                                    свяжемся
                                </div>
                            </div>
                            <form class="modal-body">
                                <input class="form-control" type="text" placeholder="Имя" required>
                                <input class="form-control" type="email" placeholder="Email" required>
                                <input class="form-control mb-40" type="tel" placeholder="Номер телефона" required>
                                <div class="custom-control custom-checkbox checkbox-terms mb-40">
                                    <input type="checkbox" class="custom-control-input" id="cb-terms-1" required>
                                    <label class="custom-control-label" for="cb-terms-1">
                                        Я соглашаюсь с политикой конфиденциальности
                                    </label>
                                </div>
                                <button class="btn btn-secondary-faded">Заказать звонок</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-2">
                    Модал 2
                </button>
                <div class="modal fade" id="modal-2" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title text-primary mb-3">1C: Предприятие 8.ERP</h2>
                                <div class="font-weight-bold" style="font-size: 24px">115 000 р.</div>
                            </div>
                            <form class="modal-body">
                                <div style="font-size: 16px" class="d-flex">
                                    <div class="mx-4">
                                        <div class="custom-control custom-radio mb-4">
                                            <input type="radio" id="key-hasp" name="key-type"
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="key-hasp">HASP</label>
                                        </div>
                                        <div class="custom-control custom-radio mb-40">
                                            <input type="radio" id="key-digital" name="key-type"
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="key-digital">цифровой</label>
                                        </div>
                                    </div>
                                    <div class="mx-4">
                                        <div class="custom-control custom-radio mb-4">
                                            <input type="radio" id="key-physical" name="key-type"
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="key-physical">физический</label>
                                        </div>
                                        <div class="custom-control custom-radio mb-40">
                                            <input type="radio" id="key-license" name="key-type"
                                                   class="custom-control-input">
                                            <label class="custom-control-label" for="key-license">лицензионный
                                                ключ</label>
                                        </div>
                                    </div>
                                </div>
                                <input class="form-control" type="text" placeholder="Имя" required>
                                <input class="form-control" type="email" placeholder="Email" required>
                                <input class="form-control mb-40" type="tel" placeholder="Номер телефона" required>
                                <div class="custom-control custom-checkbox checkbox-terms mb-40">
                                    <input type="checkbox" class="custom-control-input" id="cb-terms-2" required>
                                    <label class="custom-control-label" for="cb-terms-2">
                                        Я соглашаюсь с политикой конфиденциальности
                                    </label>
                                </div>
                                <button class="btn btn-secondary-faded">Заказать звонок</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-3">
                    Модал 3
                </button>
                <div class="modal fade" id="modal-3" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title text-primary mb-3">ИТС ПРОФ Комфортный</h2>
                                <div class="font-weight-bold" style="font-size: 24px">
                                    5 месяцев <strong>15 000 р.</strong>
                                </div>
                            </div>
                            <form class="modal-body">
                                <input class="form-control" type="text" placeholder="Имя" required>
                                <input class="form-control" type="email" placeholder="Email" required>
                                <input class="form-control mb-40" type="tel" placeholder="Номер телефона" required>
                                <div class="custom-control custom-checkbox checkbox-terms mb-40">
                                    <input type="checkbox" class="custom-control-input" id="cb-terms-3" required>
                                    <label class="custom-control-label" for="cb-terms-3">
                                        Я соглашаюсь с политикой конфиденциальности
                                    </label>
                                </div>
                                <button class="btn btn-secondary-faded">Заказать звонок</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-4">
                    Модал 4
                </button>
                <div class="modal fade modal-thank" id="modal-4" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title text-primary">Спасибо за обращение!</h2>
                                <div>
                                    Ваша заявка принята и скоро мы с вами свяжемся!
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>
