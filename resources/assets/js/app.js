function initGlide(element) {
    let options = {};
    let perView = element.getAttribute("data-glide-perview");
    let type = element.getAttribute("data-glide-type");
    let gap = element.getAttribute("data-glide-gap");
    let slidesCount = element.querySelectorAll('.glide__slide').length;
    options.gap = gap ? gap : 10;
    options.type = type ? type : 'carousel';
    if (perView) {
        options.perView = perView;
        options.breakpoints = {
            768: {
                perView: 1
            },
        }
    } else {
        options.breakpoints = {
            768: {
                perView: 1
            },
            992: {
                perView: 2
            },
            3000: {
                perView: 3
            }
        }
    }
    let glide = new Glide(element, {
        startAt: 0,
        ...options
    }).mount();

    let indicesSpan = element.querySelector('.slider-indices');
    if (indicesSpan) {
        indicesSpan.innerHTML = `1/${slidesCount}`;
        glide.on('run', function () {
            indicesSpan.innerHTML = `${glide.index + 1}/${slidesCount}`;
        });
    }
}

/**
 function for init custom input range
 @param input DOM Element input[type=range]
 */
function initCustomRange(input) {
    let coef = 14 / input.offsetWidth * 0.5;
    input.oninput = function () {
        let value = (input.value - input.min) / (input.max - input.min);
        value -= (value >= 0.5 ? value : -(1 - value)) * coef;
        input.style.backgroundImage = [
            `-webkit-gradient(linear, left top, right top, color-stop(${value}, #F5A623), color-stop(${value}, #ebebeb))`
        ].join('');
    };
    input.dispatchEvent(new Event('input'));
}

document.addEventListener('DOMContentLoaded', function () {
        let inputs = document.querySelectorAll('input[type=range]');
        for (let i = 0; i < inputs.length; i++) {
            initCustomRange(inputs[i]);
        }

        let glides = document.querySelectorAll('.glide');
        for (let i = 0; i < glides.length; i++) {
            initGlide(glides[i]);
        }
    }
);

window.dispatchDelayedResize = function () {
    setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
    }, 300);
};

/**
 used for right glider initialization after fade in
 example on page about
 */
window.goToSpecialistCard = function (anchor) {
    document.querySelector(`[name=${anchor}]`)
        .scrollIntoView({block: "start", behavior: "smooth"});
    setTimeout(function () {
        window.dispatchEvent(new Event('resize'));
    }, 300);
};

let forms = document.getElementsByClassName('form-validation');
for (let form of forms) {
    form.addEventListener('submit', function (event) {
        event.preventDefault();
        if (!this.checkValidity()) {
            this.classList.toggle('form-validation__show-errors', true);
        } else {
            let formContent = this.getElementsByClassName('form__content')[0];
            let formSuccessMsg = this.getElementsByClassName('form__success-message')[0];
            if (formContent && formSuccessMsg) {
                formContent.style.display = 'none';
                formSuccessMsg.style.display = 'block';
            }
        }
    }, false);
}
